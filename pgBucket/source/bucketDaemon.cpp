/*
 * bucketDaemon.cpp
 *
 *      This source file handle with all the pgBucket daemon operations.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/bucketDaemon.cpp
 */

#include "../include/bucketDaemon.h"

extern bool isDebug;
extern atomic<long> dispatchDelay;
extern atomic<int> dispatchLimit;
atomic<bool> isSigterm = {false};
extern logger logWriter;
extern configParams *settings;

long bucketDaemon::secSpend = 0;
schDbOps *bucketDaemon::schOps = nullptr;
hashTable *bucketDaemon::jHash = nullptr;
hashTable *bucketDaemon::eHash = nullptr;
jobsRunner *bucketDaemon::jRunner = nullptr;
jobsQueue *bucketDaemon::jobQ = nullptr;
socketServer *bucketDaemon::sockServer = nullptr;
connPooler *bucketDaemon::dbConPool = nullptr;
string bucketDaemon::pidFileLoc = "";
int bucketDaemon::pidFd;
pthread_t bucketDaemon::maintThreadId;
mutex bucketDaemon::resourceLocker;

bucketDaemon::bucketDaemon (schDbOps *ops, hashTable *jH, hashTable *eH, postgres *conn, configParams *confParams, int fd)
{
    bucketDaemon::schOps  = ops;
    bucketDaemon::jHash   = jH;
    bucketDaemon::eHash = eH;
    // Push the number of concurrent database connections to centralConnPool
    //
    int cnt;

    if (!string2Int (*settings->getConfigParam ("pgbucket_dbpool_connections"), cnt, 1))
        throw schExcp (ERROR, "Unable to process the number of dbpool connections");

    bucketDaemon::dbConPool = new connPooler (conn, cnt);
    bucketDaemon::pidFileLoc = *settings->getConfigParam ("pid_dir") + "/pgBucket.pid";
    bucketDaemon::sockDir = *settings->getConfigParam ("sock_dir");
    bucketDaemon::pidFd = fd;
    bucketDaemon::maintThreadId = pthread_self();
}
/*
 * reloadSettings:
 * 		This method will overwrite the configuration settings
 */
void bucketDaemon::reloadSettings (int signal)
{
    LOG ("Received signal " + std::to_string (signal), DDEBUG);

    if (settings != nullptr) {
        settings->reloadSettings();

        if (*settings->getConfigParam ("debug") == "on")
            isDebug = true;
        else
            isDebug = false;

        dispatchDelay = stoi (*settings->getConfigParam ("dispatch_delay"));
        dispatchLimit = stoi (*settings->getConfigParam ("dispatch_limit"));

    } else
        LOG ("Unable to reload settings.", ERROR);
}
/*
 * reFresh:
 *
 *      This method will refresh the current queue, buckets and job hash.
 *      This will be invoked automatically everyday at 00:00:00.
 */

void bucketDaemon::reFresh (int signal)
{
    time_t startTime = getMyClockEpoch();
    LOG ("Received signal " + std::to_string (signal), DDEBUG);
    // While instance is refreshing the hash, job queue and buckets
    // we need to skip the jobs to enter into job queue. Since, we are refreshing the whole pgBucket instance.
    //
    jHash->setIsResyncInProgress (true);
    hashTable *tmpJHash, *tmpEHash;
    {
        LckGaurd lock (resourceLocker);
        LOG ("Instance is updating with today's information", NOTICE);
        jobQ->cleanJobQ();
        delete jobQ;
        jobQ = nullptr;
        //Assigning old hash to temp hash and populating jHash with fresh set of jobs
        //
        tmpJHash = jHash;
        tmpEHash = eHash;
        jHash = new jobsHashTable (JOB);
        eHash = new evntsHashTable (EVENT);
        schOps = new schDbOps (dbConPool, jHash);
        time_t stime = getMyClockEpoch();
        jHash = schOps->pushJobs2Hash();
        time_t etime = getMyClockEpoch();

        // Setting jobHash time over head
        //
        if (jHash != nullptr)
            jHash->setJobHashTOH ((int) (etime - stime));
        else
            throw schExcp (ERROR, "Pushing jobs into hash got failed.");

        // Populate jobQueue, with jobIds
        //
        stime = getMyClockEpoch();
        jobQ = new jobsQueue (jHash->getHashTable());
        etime = getMyClockEpoch();

        if (jobQ != nullptr) {
            // Do update bucket pointers
            //
            jobQ->setJobQueueTOH ((int) (etime - stime));
            jobQ->doUpdBuckPntrs();
            jobQ->doUpdBuckCounters();
            LOG ("Instance update is completed", NOTICE);
        }

        // Reset secSpend to 0
        //
        secSpend = 0;
    }
    //Let us make this process as concurrent
    //
    thread conCurrProc ([tmpJHash, tmpEHash]() {
        if (tmpJHash != nullptr) {
            // Once new job hash is instantiated, then wait until the current running jobs get complete.
            //
            time_t stime = getMyClockEpoch();
            tmpJHash->cleanHashTable();
            delete tmpJHash;
            LOG ("Concurrent refresh of job hash table is completed and it took " + std::to_string (getMyClockEpoch() - stime) + " seconds.", NOTICE);
        }

        if (tmpEHash != nullptr) {
            // Once new event hash is instantiated, then wait until the current running event jobs get complete.
            //
            time_t stime = getMyClockEpoch();
            tmpEHash->cleanHashTable();
            delete tmpEHash;
            LOG ("Concurrent refresh of event hash table is completed and it took " + std::to_string (getMyClockEpoch() - stime) + " seconds.", NOTICE);
        }
    });
    conCurrProc.detach();
    LOG ("Instance refresh is completed and it took " + std::to_string (getMyClockEpoch() - startTime) + " seconds.", NOTICE);
}

/*
 * doUpdateHash:
 *
 *      This method update the hash, buckets and queue if we do any job modifications on fly.
 */
void bucketDaemon::doUpdateHash (int signal)
{
    LOG ("Received signal " + std::to_string (signal), DDEBUG);

    if (maintThreadId != pthread_self()) {
#ifdef _GNU_SOURCE
        LOG ("This thread " + std::to_string (pthread_self()) + " is not a worker thread, hence ignoring signal(SIGUSR1)", NOTICE);
#endif
        return;
    }

    time_t startTime = getMyClockEpoch();
    LckGaurd lock (resourceLocker);
    vector<vector<string>> result;
    std::unordered_map<string, size_t> fields;
    schOps->syncHashWithDb (result, fields);
    string emsg;

    /*
     * Updating jobQueue
     *
     * TODO:
     *      Try to embed the jobQueue update in a single abstraction.
     */
    for (auto row = result.begin(); row != result.end(); row++) {
        try {
            // If the updated entry belong to JOB class, then only update the jobQ and also update the job's next run.
            //
            if (row->at (fields["jobclass"]) == "JOB") {
                jobIdPosPair jobIdPos;

                if (row->at (fields["syncop"]) == "D"
                    || row->at (fields["syncop"]) == "UD") {
                    // Delete job from jobQ if it is Delete/Update
                    //
                    if (jobQ->getFront() != nullptr)
                        jobQ->delJobFrmQueue ((size_t) stoi (row->at (fields["jobid"])));
                }

                if (row->at (fields["syncop"]) == "I"
                    || row->at (fields["syncop"]) == "UI") {
                    // Insert new jobid and it's hashPosition
                    //
                    jobIdPos.hashPos = jHash->getJobIdPos ((size_t) stoi (row->at (fields["jobid"])));
                    jobIdPos.jobId = (size_t) stoi (row->at (fields["jobid"]));
                    time_t t = getMyClockEpoch();
                    long nextRun = jobQ->setJobNextRun (jHash->getJob (jobIdPos.hashPos), t);

                    if (nextRun <= -1)
                        LOG ("Might be the Job id ->" + std::to_string (jobIdPos.jobId) + " is disabled/deleted/not configured to run today", NOTICE);
                    else
                        jobQ->insJob2Queue (jobIdPos, t, nextRun);
                }
            }

            dbConPool->execDMLstmt (
                "DELETE FROM _schedule_pgbucket_.jobsynchash WHERE jobid = "
                + row->at (fields["jobid"]) + " AND syncop = '"
                + row->at (fields["syncop"]) + "'", emsg);

            LOG ("Updated the Job id ->" + row->at (fields["jobid"]) + " properties", NOTICE);

        } catch (exception &e) {
            e.what();
            continue;
        }
    }

    LOG ("Hash updation is completed, and it took " + std::to_string (getMyClockEpoch() - startTime) + " seconds", NOTICE);
}

/*
 * handleIntrKill:
 *
 *      This method handle the external termination request of the pgBucket.
 */

void bucketDaemon::handleIntrKill (int s)
{
    string msg = "Received signal ";
    string emsg = "";

    switch (s) {
        case SIGINT:
            msg += "SIGINT";
            break;

        case SIGTERM:
            msg += "SIGTERM";
            break;

        case SIGFPE:
            msg += "SIGFPE";

            if (errno != 0)
                emsg = string (strerror (errno));

            break;

        case SIGSEGV:
            msg += "SIGSEGV";

            if (errno != 0)
                emsg = string (strerror (errno));

            break;

        case SIGABRT:
            msg += "SIGABRT";

            if (errno != 0)
                emsg = string (strerror (errno));

            break;

        case SIGILL:
            msg += "SIGILL";

            if (errno != 0)
                emsg = string (strerror (errno));

            break;

        default:
            msg += std::to_string (s);
    }

    LOG (msg, NOTICE);

    // Log the error message, which caused the daemon to stop.
    //
    if (!emsg.empty())
        LOG (emsg, ERROR);

    // Let us make all jobs don't enter into the jobQ.
    // Since, we are terminating the bucket daemon here.
    //
    jobQ->setIsRefreshStarted (true);
    LOG ("Stopping pgBucket Daemon..", DETAIL);
    emsg = "";
    // Let us enable the isSigterm flag, which stops the job runner to stop the further job dispatches.
    //
    isSigterm = true;
    LckGaurd lock (resourceLocker);

    if (jobQ != nullptr) {
        jobQ->cleanJobQ();
        // Deleting jobQ
        delete jobQ;
        jobQ = nullptr;
    }

    LOG ("Cleared job queue", NOTICE);

    if (jHash != nullptr) {
        jHash->cleanHashTable();
        // Deleting jobHash
        delete jHash;
        jHash = nullptr;
    }

    LOG ("Cleared job hash table", NOTICE);

    if (eHash != nullptr) {
        eHash->cleanHashTable();
        delete eHash;
        eHash = nullptr;
    }

    LOG ("Cleared event hash table", NOTICE);

    if (schOps != nullptr) {
        // Deleting schOps
        delete schOps;
        schOps = nullptr;
    }

    if (
        dbConPool->execDMLstmt ("UPDATE _schedule_pgbucket_.jobstatus SET jobendtime = now(), result ='', error = 'unknown', status='E' WHERE status ='R' and jobendtime IS NULL", emsg)
        &&
        dbConPool->execDMLstmt ("UPDATE _schedule_pgbucket_.jobstatus SET jobendtime = now(), result ='', error = 'unknown', status='S' WHERE status ='R' and jobendtime IS NOT NULL", emsg)
    ) {
        LOG ("Updated running jobs as failed", DDEBUG);

    } else {
        LOG ("Unable to set the running jobs as failed", DDEBUG);
    }

    dbConPool->clearPool();
    LOG ("Cleared connection pool", NOTICE);
    unlockFile (pidFd);				// Release lock on pid file
    unlink (pidFileLoc.c_str()); 	// Remove pid file
    _exit (0);
}

/*
 * daemonInitStuff:
 *
 *      This is method is a helper function to the daemonIT process.
 */
void bucketDaemon::daemonInitStuff()
{
    // Change working directory to root
    //
    if (chdir ("/") == -1)
        throw schExcp (ERROR, "Unable to change the process current working directory. Error: " + string (strerror (errno)));

    // Closing stdin, stdout and stderror fds
    //
    close (STDIN_FILENO);
    close (STDOUT_FILENO);
    close (STDERR_FILENO);

    if (open ("/dev/null", O_RDONLY | O_NOCTTY | O_NOFOLLOW) != 0 ||
        open ("/dev/null", O_WRONLY | O_NOCTTY | O_NOFOLLOW) != 1 ||
        open ("/dev/null", O_WRONLY | O_NOCTTY | O_NOFOLLOW) != 2)
        throw schExcp (ERROR, "Unable to route the daemon stdin,stdout and stderr to /dev/null. Error: " + string (strerror (errno)));

    // Clearing umask too
    //
    umask (0);
}

/*
 * daemonIt:
 *
 *      This method makes the process as daemon process.
 */
void bucketDaemon::daemonIt()
{
    if (getppid() == 1) {
        // If this daemon's parent process is 1, then
        // it was initiated by the init process.
        // So, no need to take care of controlling terminals and
        // process groups.
        //
        daemonInitStuff();
        return;

    } else {
        // Ignore terminal signals
        //
#ifdef SIGTTOU
        signal (SIGTTOU, SIG_IGN); // TTY output signal
#endif
#ifdef SIGTTIN
        signal (SIGTTIN, SIG_IGN); // TTY input signal
#endif
#ifdef SIGTSTP
        signal (SIGTSTP, SIG_IGN); // TTY stop signal
#endif

        // Close parent process
        //
        if (fork() != 0)
            _exit (0);

        // Continue to the child process,
        // and clear it's controlling terminal,
        // and remove this process from process group
        //
        int fd;

        if ((fd = open ("/dev/tty", O_RDWR)) >= 0) {
            ioctl (fd, TIOCNOTTY, 0);
            close (fd);
        }

        setpgrp();
        setsid();

        if (fork() != 0)
            _exit (0);   // Second child, which is a non process group leader.

        daemonInitStuff();
        return;
    }
}

/*
 * pushBucketPointers:
 *
 *      This method will be updating the bucket pointers.
 */

void bucketDaemon::pushBucketPointers (jobsQueue **from, jobsQueue **to, jobSchBuckets frmSchBucket, jobSchBuckets toSchBucket)
{
    time_t currentEpoch = getMyClockEpoch();

    if ((*from) != NULL) {
        // If "From" bucket is not empty, then traverse to "to" bucket,
        // and set all job's schbucket value as toSchBucket
        //
        LOG ("From bucket is not null", DDEBUG);

        // If "To" bucket is null then traverse the whole job queue, and adjust the "From" bucket position.
        //
        if ( (*to) == NULL) {
            LOG ("To bucket is null", DDEBUG);

            while ( (*from)->getJobSchBucket() == frmSchBucket ) {
                if ( ((*from)->getJobsNextRun() - currentEpoch) <= toSchBucket ) {
                    (*from)->setJobSchBucket (toSchBucket);
                    LOG ("From bucket pointer is moving forward. Since, To bucket is empty", DDEBUG);

                } else {
                    LOG ("From bucket pointer reaches until ToBucket pointer fits", DDEBUG);
                    break;
                }

                if ((*from)->getNext() == NULL) {
                    LOG ("From bucket pointer reaches to queue end.", DDEBUG);
                    break;
                }

                (*from) = (*from)->getNext();
            }
        }

        // If not, traverse from FROM => TO bucket
        // and set all job sch bucket as toSchBucket
        //
        else {
            LOG ("To bucket is not null", DDEBUG);

            while ( (*from) != (*to) ) {
                // Moving FROM bucket pointer towards TO bucket values
                //
                if ( ((*from)->getJobSchBucket() == frmSchBucket) && ((*from)->getJobsNextRun() - currentEpoch) <= toSchBucket) {
                    (*from)->setJobSchBucket (toSchBucket);
                    LOG ("From bucket pointer moving towards To bucket pointer.", DDEBUG);

                } else if ( ((*from)->getJobSchBucket() == frmSchBucket) && ((*from)->getJobsNextRun() - currentEpoch) > toSchBucket) {
                    LOG ("From bucket pointer reached it's position.", DDEBUG);
                    break;
                }

                (*from) = (*from)->getNext();
            }

            // Check if we reached the TO bucket.
            // If so, modify the FROM bucket pointer to TO bucket pointer
            //
            if ( (*from) == (*to) )
                (*from) = (*to);
        }

    } else {
        LOG ("From bucket is null", DDEBUG);
        jobsQueue *tmp = (*to);
        jobSchBuckets tmpBucket;

        while (tmp) {
            tmpBucket = tmp->getJobSchBucket();

            if (tmpBucket == frmSchBucket && tmp->getJobsNextRun() - currentEpoch <= toSchBucket) {
                tmp->setJobSchBucket (toSchBucket);
                (*from) = tmp;
                LOG ("To bucket pointer is moving. Since, From bucket pointer is empty", DDEBUG);

            } else if (tmpBucket == frmSchBucket && tmp->getJobsNextRun() - currentEpoch > toSchBucket) {
                LOG ("To bucket pointer halts here.", DDEBUG);
                break;
            }

            if (tmpBucket == frmSchBucket + LTE_1H) {
                LOG ("Item reaches to it's next bucket. Hence, halting here.", DDEBUG);

                if (tmp->getJobsNextRun() - currentEpoch <= frmSchBucket && (*from) == NULL) {
                    LOG ("Item fits with from bucket. Hence, pointing from bucket with this item", DDEBUG);
                    (*from) = tmp;
                }

                break;
            }

            tmp = tmp->getNext();
        }
    }

    if ((*from) != NULL) {
        // Check whether from bucket reaches to it's next bucket
        // If not, set the FROM bucket to null
        //
        if ((*from)->getJobSchBucket() != frmSchBucket && (*from)->getJobSchBucket() != frmSchBucket + LTE_1H) {
            (*from) = nullptr;
            LOG ("From bucket pointer is not pointing to next bucket and it self. Hence, setting this as nullptr", DDEBUG);

        } else if ( (*from)->getJobsNextRun() - currentEpoch > frmSchBucket) {
            (*from) = nullptr;
            LOG ("From bucket pointer won't fit here. Since, pointer reaches to the item which nextrun is greater that it's bucket value", DDEBUG);
        }
    }
}

/*
 * updateBuckets:
 *
 *      After spending 3600 seconds, let us update the bucket pointers like
 *      _1hBucket = List of _1hBucket + List of _2hBucket
 *      _2hBucket = _3hBucket
 *      _3hBucket = _gt3hBucket
 *      _gt3hBucket = Move pointer to the first job, which nextrun >LTE_3H
 */

void bucketDaemon::updateBuckets()
{
    // Let us disable all signals for this thread.
    // Since, at 00 hour we may raise hour change signal
    // and day change signal at the same time.
    //
    if (!disableSignals()) {
        LOG ("Unable to disable the maintenance signals to the pgBucket's bucket update process", ERROR);
        return;
    }

    while (1) {
        // If the current HOUR is 00 then don't reload buckets.
        // Since, the day change single will take care of reloading buckets.
        //
        if ( secSpend >= LTE_1H && getTimeElement (HOUR) != 0 ) {
            LckGaurd lock (resourceLocker);
            time_t currentEpoch = getMyClockEpoch();
            secSpend = 0;
            LOG ("Initiating bucket pointers update. Since, we spend " + std::to_string (LTE_1H) + " many seconds..", DDEBUG);
            jobsQueue **_1hBucket = jobQ->getBucketStart (LTE_1H);
            jobsQueue **_2hBucket = jobQ->getBucketStart (LTE_2H);
            jobsQueue **_3hBucket = jobQ->getBucketStart (LTE_3H);
            jobsQueue **_gt3hBucket = jobQ->getBucketStart (GT_3H);
            LOG ("Appending 2 hour bucket jobs into 1 hour bucket, and updating 2 hour bucket pointer as 3 hour bucket.", DDEBUG);
            pushBucketPointers (_2hBucket, _3hBucket, LTE_2H, LTE_1H);

            // Check whether 1 hour bucket is empty
            // If so, update the 1 hour bucket with front
            //
            if ( (*_1hBucket) == nullptr && jobQ->getFront() != nullptr ) {
                if (jobQ->getFront()->getJobBucket() == LTE_1H)
                    (*_1hBucket) = jobQ->getFront();
            }

            LOG ("Appending 3 hour bucket jobs into 2 hour bucket, and updating 3 hour bucket pointer as gt3hour bucket.", DDEBUG);
            pushBucketPointers (_3hBucket, _gt3hBucket, LTE_3H, LTE_2H);
            jobsQueue **tmp = _gt3hBucket;

            // Now, let us traverse the gt3h bucket, and update the gt3h bucket pointer
            //
            while (*tmp) {
                if ((*tmp)->getJobsNextRun() - currentEpoch >= GT_3H) {
                    *_gt3hBucket = *tmp;
                    break;
                }

                // Check if by chance 3 hour bucket is empty.
                // If it is, then set the 3 hour bucket to this element.
                //
                if ((*_3hBucket) == NULL) {
                    LOG ("Found 3 hour bucket pointer is empty during GT3H bucket pointer moves.", DDEBUG);
                    LOG ("Hence, pointing 3 hour bucket pointer to this item.", DDEBUG);
                    (*_3hBucket) = (*tmp);
                }

                (*tmp)->setJobSchBucket (LTE_3H);
                (*tmp) = (*tmp)->getNext();
            }

            // Now, let us update the bucket counters..
            //
            jobQ->doUpdBuckCounters();
        }

        // Sleep 200ms and then validate the hour change event
        //
        usleep (200000);
    }
}

/*
 * runDaemon:
 *
 *      This method is responsible to make the pgBucket as daemon, and also this will be the worker thread for
 *      all the maintenance operations like "Refresh buckets on everyday", "Refresh buckets when any job inserted/updated" ...
 */
void bucketDaemon::runDaemon (runmode mode)
{
    try {
        if (mode == BACKGROUND)
            // Make process as daemon
            //
            daemonIt();

        string pid = std::to_string (getpid());

        if (write (pidFd, pid.c_str(), pid.length()) == -1)
            throw schExcp (ERROR, "Unable to write pid into pid file." + string (strerror (errno)));

        // Clean syncHash table in DB,
        // before we load any jobs to hash
        //
        if (schOps->cleanSyncHashDbTable() == false)
            throw schExcp (ERROR, "Unable to clean syncHashTable during daemon start");

        // Mark previous running jobs status as Fail
        //
        if (schOps->markStaleRunStatusAsFail() == false)
            throw schExcp (ERROR, "Unable to mark the stale job's run status as fail");

        // Update the dispatcher settings
        //
        dispatchDelay = stoi (*settings->getConfigParam ("dispatch_delay"));
        dispatchLimit = stoi (*settings->getConfigParam ("dispatch_limit"));
        // Trying to hold a lock on pid file
        //
        int lckStatus = lockFile (pidFd, F_WRLCK);

        if (lckStatus == -1)
            throw schExcp (ERROR, "Unable to acquire lock on pid file." + string (strerror (errno)));
        else if (lckStatus != 1)
            throw schExcp (ERROR, "Lock on pid file is already acquired by the pid " + std::to_string (lckStatus));

        jHash = schOps->getJobHash();
        // Detaching thread which will do listen/response to local socket
        //
        sockServer = new socketServer (sockDir);
        sockServer->initServerSock();
        thread listenAndRespond ([this]() {
            sockServer->listenAndProcessMsg (this);
        });
        listenAndRespond.detach();
        // Set daysBeginEpoch, and detach update day begin epoch thread
        //
        set2DayBeginEpoch();
        maintThreadId = pthread_self();
        thread updDayBeginngEpoch (raiseRefreshSignal, maintThreadId);
        updDayBeginngEpoch.detach();
        // Update bucket pointers for every 3600 seconds
        //
        thread bucketUpd (bucketDaemon::updateBuckets);
        bucketUpd.detach();
        // Push today's job into the jobHash table
        //
        time_t stime, etime;
        stime = getMyClockEpoch();
        jHash = schOps->pushJobs2Hash();
        etime = getMyClockEpoch();

        // Setting jobHash time over head
        //
        if (jHash != nullptr)
            jHash->setJobHashTOH (int (etime - stime));
        else
            throw schExcp (ERROR, "Pushing jobs into hash got failed.");

        // Push all event jobs into event hash table
        //
        eHash = schOps->pushEvnts2Hash();
        // Populate jobQueue, with jobIds
        //
        stime = getMyClockEpoch();
        jobQ = new jobsQueue (jHash->getHashTable());
        etime = getMyClockEpoch();

        // Setting jobQ time over head
        //
        if (jobQ != nullptr)
            jobQ->setJobQueueTOH (int (etime - stime));
        else
            throw schExcp (ERROR, "Pushing jobs from hash to queue is failed.");

        // Update bucket pointers
        //
        jobQ->doUpdBuckPntrs();
        thread runDeamon (bucketDaemon::start);
        runDeamon.detach();

        if (!enableSignals()) {
            LOG ("Unable to enable the maintenance signals for the pgBucket's worker process.", ERROR);
            return;
        }

        // Enabling all required signals to this worker thread
        //
        signal (SIGINT, handleIntrKill);
        signal (SIGTERM, handleIntrKill);
        signal (SIGABRT, handleIntrKill);
        signal (SIGFPE, handleIntrKill);
        signal (SIGSEGV, handleIntrKill);
        signal (SIGILL, handleIntrKill);
        // Assigning signal handler functions
        //   SIGUSR1 => Update hash/queue if any job modification happens
        //   SIGUSR2 => External instance refresh
        //   SIGALRM => Internal instance refresh
        //   SIGHUP => Reload settings
        //
        signal (SIGUSR1, doUpdateHash);
        signal (SIGUSR2, reFresh);
        signal (SIGALRM, reFresh);
        signal (SIGHUP, reloadSettings);

        while (1) {
            // Let this be the worker thread for all our signals.
            //
            maintThreadId = pthread_self();
#ifdef _GNU_SOURCE
            LOG ("Worker thread for the signals is " + std::to_string (maintThreadId), DDEBUG);
#endif
            sleep (LTE_1H);
        }

    } catch (exception &e) {
        throw;
    }
}

/*
 * start:
 *
 *      This method collect the jobs from the queue which are ready to run, and dispatch them accordingly.
 */
void bucketDaemon::start()
{
    time_t runEpoch, oldTick, newTick = -1;
    vector<jobIdPosPair> *jIds2Run;
    oldTick = getMyClockEpoch();
    int timeOverHead = jHash->getJobHashTOH() + jobQ->getJobQueueTOH();

    if (!disableSignals()) {
        LOG ("Unable to disable the maintenance signals to the pgBucket's dispatcher process.", ERROR);
        return;
    }

    //setStackLimit(50 * 1024 * 1024);
    do {
        if ( newTick > oldTick) {
            runEpoch = oldTick = getMyClockEpoch();
            LckGaurd lock (resourceLocker);

            // Checking resource pointers
            //
            if (jobQ != nullptr && jHash != nullptr && dbConPool != nullptr) {
                // If the 1 hour bucket has at least one job in it,
                // then only get the list of job ids to dispatch..
                //
                if (jobQ->getSchHoursCount (LTE_1H) >= 1) {
                    //Adding job hash and job queue calculation time overheads.
                    //This overhead value will be reduced from the job's actual dispatch time.
                    //
                    jIds2Run = jobQ->getJids2Dispatch (runEpoch,
                                                       timeOverHead);
                    jRunner = new jobsRunner (jIds2Run, jHash, eHash, jobQ, dbConPool);
                    jRunner->dispatch();
                    //Clear jRunner, jIds2Run
                    //
                    delete jRunner;
                    jRunner = nullptr;
                    delete jIds2Run;
                    jIds2Run = nullptr;
                    // Update the time overhead value
                    //
                    timeOverHead = int (getMyClockEpoch() - runEpoch);

                } else
                    LOG ("No jobs found in 1 hour bucket.", DDEBUG);
            }

            bucketDaemon::secSpend++;
            LOG ("Buckets will be updated in next " + std::to_string (LTE_1H - secSpend) + " seconds.", DDEBUG);
            LOG ("Instance refresh will be triggered in next " + std::to_string (get2DaysBeginEpoch() + DAYSECS - getMyClockEpoch()) + " seconds.", DDEBUG);
            LOG ("1 hour bucket list count is: " + std::to_string (jobQ->getSchHoursCount (LTE_1H)), DDEBUG);
            LOG ("2 hour bucket list count is: " + std::to_string (jobQ->getSchHoursCount (LTE_2H)), DDEBUG);
            LOG ("3 hour bucket list count is: " + std::to_string (jobQ->getSchHoursCount (LTE_3H)), DDEBUG);
            LOG ("GT3 hour bucket list count is: " + std::to_string (jobQ->getSchHoursCount (GT_3H)), DDEBUG);
        }

        // If isSigterm flag enable, do not fetch any more job sets
        //
        if (isSigterm)
            break;

        // Sleep 500ms
        //
        usleep (500000);
        newTick = getMyClockEpoch();
    } while (1);
}

/*
 * doActions:
 *
 *      This method process the request which came from the socket.
 */
void bucketDaemon::doActions (daemonActions action, size_t jobId, int extendedCols)
{
    jobIdPosPair jobPair;
    jobs *job = nullptr;
    string msg;
    string jHashTable, eHashTable, jobQueue;
    bool isForce = true;
    bool isFail = false;
    bool isExtended = false;
    bool isEventJob = false;
    runMode mode = EXPLICIT_FORCE;

    // Daemon do not require jobID for printing it's hash/queue/pool instances
    //
    if ( jobId != 0 ) {
        LckGaurd lock (resourceLocker);
        int jobPos = jHash->getJobIdPos (jobId);

        if (jobPos == -1) {
            // Check for the job in event hash table
            //
            jobPos = eHash->getJobIdPos (jobId);

            if (jobPos == -1) {
                msg = "Job id ->" + std::to_string (jobId) + " not found in the hash.";
                sockServer->sendMessage (msg);
                return;
            }

            isEventJob = true;
        }

        jobPair.jobId = jobId;
        jobPair.hashPos = (unsigned int) jobPos;

        //Try to get the job from the job/event hash.
        //If it fails to get the job from the hash position, then return from here
        //
        try {
            if (!isEventJob)
                job = jHash->getJob (jobPair.hashPos);
            else
                job = eHash->getJob (jobPair.hashPos);

        } catch (exception &e) {
            msg = string (e.what());
            sockServer->sendMessage (msg);
            return;
        }

        if (job == nullptr) {
            msg = "Unable to find the Job id ->" + std::to_string (jobId) + " from job/event hash tables.";
            sockServer->sendMessage (msg);
            return;
        }

    } else if (jobId == 0 && ! (action == PRINT_EVENT_HASH || action == PRINT_JOB_QUEUE || action == PRINT_JOB_HASH || action == PRINT_CON_POOL || action == EPRINT_CON_POOL || action == EPRINT_JOB_HASH || action == EPRINT_JOB_QUEUE || action == EPRINT_EVENT_HASH)) {
        msg = "Invalid action for the Job id ->0";
        sockServer->sendMessage (msg);
        return;
    }


    // For few of the case actions, we may need resource locker. Since, we are doing an instant action what daemon about to do.
    //
    switch (action) {
        case ENABLE:
            msg = "Trying to enable the Job id ->" + std::to_string (jobId);
            sockServer->sendMessage (msg);

            job->lockJob();

            // Check whether the job is already enabled.
            //
            if (job->getJobStatus()) {
                msg = "Job id ->" + std::to_string (jobId) + " is enabled already..";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);

            } else {
                if (job->isJobDeleted()) {
                    msg = "Invalid action against a deleted Job id ->" + std::to_string (jobId);
                    sockServer->sendMessage (msg);
                    LOG (msg, NOTICE);

                } else {
                    LckGaurd lock (resourceLocker);
                    // Now, enable the job, and reset it's auto disable failure counter.
                    //
                    job->setJobStatus (true);
                    job->resetJobFailCounter();

                    // If the job is not belong to event hash, then push this job into the job Queue.
                    //
                    if (!isEventJob) {
                        long nextRun = jobQ->setJobNextRun (job, getMyClockEpoch());

                        if (! (nextRun <= -1)) {
                            jobIdPosPair jIdPos;
                            jIdPos.jobId = job->getJobID();
                            jIdPos.hashPos = jHash->getJobIdPos (job->getJobID());
                            jobQ->insJob2Queue (jIdPos, getMyClockEpoch(), nextRun);
                        }
                    }

                    msg = "Successfully enabled Job id ->" + std::to_string (jobId);
                    sockServer->sendMessage (msg);
                    LOG (msg, NOTICE);
                }
            }

            job->unlockJob();
            break;

        case DISABLE:
            job->lockJob();

            // Check whether the job is alread disabled
            //
            if (!job->getJobStatus()) {
                msg = "Job id ->" + std::to_string (jobId) + " is disabled already..";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);

            } else {
                LckGaurd lock (resourceLocker);
                // Now, disable the job.
                //
                job->setJobStatus (false);
                msg = "Disabled the Job id ->" + std::to_string (job->getJobID()) + " in instance.";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);
            }

            job->unlockJob();
            break;

        case RUN_NORMAL:
            mode = EXPLICIT_NORMAL;

        case RUN_FORCE:

            // Only allow the JOB CLASS types to run explicitly
            //

            if (isEventJob) {
                msg = "Not allowed an event job to run explicitly.";
                sockServer->sendMessage (msg);
                break;
            }

            msg = "Trying to run the Job id ->" + std::to_string (jobId);
            sockServer->sendMessage (msg);

            job->lockJob();

            // Check whether the job is disabled to run.
            //
            if (mode != EXPLICIT_FORCE && (job->isJobDeleted() || !job->getJobStatus())) {
                msg = "Job id ->" + std::to_string (jobId) + " is disabled/deleted. So, skipping this activity..";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);
            }

            // Check whether the job is already running.
            // If it is, then send message as "Job is already running".
            //
            else if (job->getJobSchStatus() == RUNNING) {
                // Send message to client socket, and return from here
                //
                msg = "Job id ->" + std::to_string (jobId) + " is already running. So, skipping this activity..";
                sockServer->sendMessage (msg);
                LOG (msg, ERROR);

            } else {
                if (mode != EXPLICIT_FORCE && job->getJobSkipNextRun()) {
                    msg = "Job id ->" + std::to_string (jobId) + " has enabled skip next run flag. Hence, skipping it to run..";
                    sockServer->sendMessage (msg);
                    LOG (msg, NOTICE);
                    isFail = true;

                } else {
                    LckGaurd lock (resourceLocker);

                    try {
                        thread explicitRun (runIt(), jobPair, jHash, eHash, jobQ, dbConPool, mode);
                        explicitRun.detach();
                        msg = "Job id ->" + std::to_string (jobId) + " is dispatched.";
                        sockServer->sendMessage (msg);
                        LOG (msg, NOTICE);

                    } catch (exception &e) {
                        msg = string (e.what());
                        sockServer->sendMessage (msg);
                    }
                }
            }

            if (isFail) {
                msg = "Use \"RF\"(Run force) option to force this action\n";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);
            }

            job->unlockJob();
            break;

        case SKIP_NEXT_RUN:
            msg = "Trying to skip the Job id ->" + std::to_string (jobId) + " next run";
            sockServer->sendMessage (msg);

            job->lockJob();

            if (job->getJobSkipNextRun() == true) {
                msg = "Job id ->" + std::to_string (jobId) + " next run is already skipped.";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);

            } else {
                LckGaurd lock (resourceLocker);
                job->setJobSkipNextRun (true);
                msg = "Enabled skip next run to the Job id ->" + std::to_string (jobId) + "\n";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);
            }

            job->unlockJob();
            break;

        case EPRINT_JOB_HASH:
            isExtended = true;

        case PRINT_JOB_HASH:
            msg = "TABLE_START";
            sockServer->sendMessage (msg);
            jHashTable = jHash->printHashTable (isExtended, extendedCols);
            sockServer->sendMessage (jHashTable);
            msg = "TABLE_END";
            sockServer->sendMessage (msg);
            break;

        case EPRINT_EVENT_HASH:
            isExtended = true;

        case PRINT_EVENT_HASH:
            msg = "TABLE_START";
            sockServer->sendMessage (msg);
            eHashTable = eHash->printHashTable (isExtended, extendedCols);
            sockServer->sendMessage (eHashTable);
            msg = "TABLE_END";
            sockServer->sendMessage (msg);
            break;

        case EPRINT_JOB_QUEUE:
            isExtended = true;

        case PRINT_JOB_QUEUE:
            msg = "TABLE_START";
            sockServer->sendMessage (msg);
            jobQueue = jobQ->printJobQueue (isExtended, extendedCols);
            sockServer->sendMessage (jobQueue);
            msg = "TABLE_END";
            sockServer->sendMessage (msg);
            break;

        case EPRINT_CON_POOL:
            isExtended = true;

        case PRINT_CON_POOL:
            msg = "TABLE_START";
            sockServer->sendMessage (msg);
            msg = dbConPool->printConnPoolState (isExtended, extendedCols);
            sockServer->sendMessage (msg);
            msg = "TABLE_END";
            sockServer->sendMessage (msg);
            break;

        // Using SIGTERM
        //
        case STOP_NORMAL:
            isForce = false;

        // Using SIGKILL
        //
        case STOP_FORCE:

            // We do not need to acuquire a job lock to kill the job.
            // Since, job execution only release it's lock when it's execution gets complete.
            //
            // Check whether the job is running or not.
            // If it is not running, then send message as "Job is not running to stop".
            //
            if (job->getJobSchStatus() != RUNNING) {
                msg = "Job id ->" + std::to_string (jobId) + " is not running to stop.";
                sockServer->sendMessage (msg);
                LOG (msg, ERROR);

            } else {
                msg = "Initiating manual kill of Job id ->" + std::to_string (jobId) + " which is running with pid(" + std::to_string (job->getJobPid()) + ")";
                sockServer->sendMessage (msg);
                LOG (msg, NOTICE);

                try {
                    LckGaurd lock (resourceLocker);

                    if (job->getJobType() == OSLEVEL)
                        job->killJob (isForce);
                    else
                        job->killJob (isForce);

                    if (job->getJobType() == OSLEVEL)
                        msg = "Successfully send " + (isForce ? string ("SIGKILL") : string ("SIGTERM")) + " signal to the job.";
                    else
                        msg = "Successfully send SIGTERM signal to the job.";

                    sockServer->sendMessage (msg);

                } catch (exception &e) {
                    msg = e.what();
                    sockServer->sendMessage (msg);
                }
            }

            break;

        case INVALID_ACTION:
            break;

        default:
            msg = "Unknown daemon action received";
            sockServer->sendMessage (msg);
            LOG (msg, ERROR);
    }
}
