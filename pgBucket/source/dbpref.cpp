/*
 * dbpref.cpp
 *
 *      This source file has the implementation of database related jobs.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/dbpref.cpp
 */
#include "../include/dbpref.h"
/*
* dbPref:
*       Default constructor of dbPref object.
*/
dbPref::dbPref()
{
    dbUserName = "UNKOWN User";
    dbMd5Password = "Invalid Password";
    dbHostIP = "UNKOWN Host IP";
    dbType = NOTSUPPORTED;
    dbPort = 0;
}
/*
* dbPref:
*       Parameterized constructor of dbPref object.
*/
dbPref::dbPref (string dbuser, string password, string hostip, DbTypes dbtype, size_t dbport)
{
    dbUserName = dbuser, dbMd5Password = password, dbHostIP = hostip, dbType = dbtype, dbPort = dbport;
}

string dbPref::getDbHostIp()
{
    return dbHostIP;
}

string dbPref::getDbMd5Password()
{
    return dbMd5Password;
}

string dbPref::getDbUserName()
{
    return dbUserName;
}

size_t dbPref::getDbPort()
{
    return dbPort;
}

/*
* postgres:
*       Default constructor of postgres object.
*/
postgres::postgres()
{
    pgDbConn = nullptr;
    dbName = "";
    dbVersion = 0;
}
/*
* postgres:
*       Parameterized constructor of postgres object.
*/
postgres::postgres (string connStr)
{
    pgDbConn = nullptr;
    jobDbConnStr = connStr;
    dbVersion = 0;
}
/*
* postgres:
*       Parameterized constructor of postgres object.
*/
postgres::postgres (const string &dbuser, const string &password, const string &hostip, const string &dbname, DbTypes dbtype, size_t dbport) :
    dbPref (dbuser, password, hostip, dbtype, dbport)
{
    dbName = dbname;
    // TODO
    //	Update dbVersion member with the database version
    //
    dbVersion = 0;
    LOG (
        "Trying connection to PG using hostaddr = " + hostip + " dbuser = "
        + dbuser + " password = " + password +
        + " dbport = " + " application_name = pgbucket" + std::to_string (dbport), DDEBUG);
    pgDbConn =
        PQconnectdb (
            string (
                "hostaddr = " + hostip + " user = " + dbuser
                + " port = " + std::to_string (dbport)
                + " dbname = " + dbname + " password =" + password).c_str());

    if (PQstatus (pgDbConn) == CONNECTION_OK)
        LOG ("Connection established", DDEBUG);
    else {
        string err = "Connection failed. Error:";

        // Check if the errno is set to non zero value, as the database connection may fail
        // if we reach the maximum number of open files.
        //
        if (errno != 0)
            err += " " + string (strerror (errno));

        // Append libpq error message
        //
        err += " " +  string (PQerrorMessage (pgDbConn));
        pgDbConn = nullptr;
        throw schExcp (ERROR, err);
    }
}

string postgres::getDbName() const
{
    return dbName;
}

/*
* getJobDBConn:
*       Returns the postgres database connection after making a successful connection.
*/
PGconn *postgres::connectAndGetDBconn()
{
    if (jobDbConnStr.empty())
        return nullptr;

    pgDbConn = PQconnectdb ((jobDbConnStr + "?application_name=pgbucket").c_str());

    if (PQstatus (pgDbConn) == CONNECTION_OK) {
        LOG ("Job db connection is established", DDEBUG);
        return pgDbConn;
    }

    throw schExcp (ERROR, "Unable to establish job's db connection. Error: " + string (PQerrorMessage (pgDbConn)));
}
/*
* getPgConn:
*       This method returns the postgres connection.
*/
PGconn *postgres::getPgConn()
{
    if (pgDbConn == nullptr) {
        LOG ("Calling null db connection pointer", DDEBUG);
        return nullptr;

    } else
        return pgDbConn;
}
/*
* execGetScalar:
*       This method execute the query and returns the single value.
*/
string postgres::execGetScalar (string query, string &emsg)
{
    LOG ("Executing Query: " + query, DDEBUG);

    if (getPgConn() == nullptr)
        throw schExcp (ERROR, "Unable to run the query as db connection is empty");

    PGresult *res = PQexec (getPgConn(), query.c_str());

    if (PQresultStatus (res) != PGRES_TUPLES_OK) {
        emsg = PQerrorMessage (getPgConn());
        PQClearNullptr (res);
        throw schExcp (ERROR, "Unable to execute the query. Error: " + emsg);

    } else if (PQntuples (res) == 0) {
        LOG ("Query execution is OK", DDEBUG);
        PQClearNullptr (res);
        return string();

    } else {
        LOG ("Query execution is OK", DDEBUG);
        string result =  PQgetvalue (res, 0, 0);
        emsg = string();
        PQClearNullptr (res);
        return result;
    }
}

string postgres::execRunGetScalar (string query, string &emsg, pid_t &pid)
{
    LOG ("Executing Query: " + query, DDEBUG);

    pid = getDbPid();

    if (getPgConn() == nullptr)
        throw schExcp (ERROR, "Unable to run the query as db connection is empty");

    PGresult *res = PQexec (getPgConn(), query.c_str());

    if (PQresultStatus (res) != PGRES_TUPLES_OK && PQresultStatus (res) != PGRES_COMMAND_OK) {
        emsg = PQerrorMessage (getPgConn());
        PQClearNullptr (res);
        throw schExcp (ERROR, "Unable to execute the query. Error: " + emsg);

    } else if (PQntuples (res) == 0) {
        LOG ("Query execution is OK", DDEBUG);
        PQClearNullptr (res);
        return string();

    } else {
        LOG ("Query execution is OK", DDEBUG);
        string result =  PQgetvalue (res, 0, 0);
        emsg = string();
        PQClearNullptr (res);
        return result;
    }
}




/*
 * chkExecStatus:
 *      This method checks the query execution status, and return true if it was successful
 */
bool postgres::chkExecStatus (PGresult *res, PGconn *conn, string &emsg, bool isTupReturn)
{
    // Check the status of all retrival queries
    //
    if (isTupReturn) {
        if (PQresultStatus (res) != PGRES_TUPLES_OK) {
            emsg = string (PQerrorMessage (conn));
            return false;
        }

        return true;
    }

    // Check the status for all the database commands
    //
    if (PQresultStatus (res) != PGRES_COMMAND_OK) {
        emsg = string (PQerrorMessage (conn));
        return false;
    }

    return true;
}
/*
 * beginTrx:
 *      This method initiate the transaction.
 */
bool postgres::beginTrx (string &emsg)
{
    PGresult *res = nullptr;

    try {
        res = PQexec (getPgConn(), "BEGIN");

        if (chkExecStatus (res, getPgConn(), emsg, false)) {
            PQClearNullptr (res);
            return true;
        }

        throw schExcp (ERROR, "Unable to initiate the transaction. Error: " + emsg);

    } catch (exception &e) {
        if (res != nullptr)
            PQClearNullptr (res);

        emsg = e.what();
        throw;
    }
}
/*
 * endTrx:
 *      This method close the transaction.
 */
bool postgres::endTrx (string &emsg)
{
    PGresult *res = nullptr;

    try {
        res = PQexec (getPgConn(), "END");

        if (chkExecStatus (res, getPgConn(), emsg, false)) {
            PQClearNullptr (res);
            return true;
        }

        throw schExcp (ERROR, "Unable to close the transaction. Error: " + emsg);

    } catch (exception &e) {
        if (res != nullptr)
            PQClearNullptr (res);

        emsg = e.what();
        throw;
    }
}

/*
 * execDMLstmt:
 *      This method execute the given DML queries.
 */
bool postgres::execDMLstmt (string query, string &emsg)
{
    PGresult *res = nullptr;

    try {
        LOG ("Executing query: " + query, DDEBUG);
        beginTrx (emsg);
        res = PQexec (getPgConn(), query.c_str());

        if (chkExecStatus (res, getPgConn(), emsg, false)) {
            PQClearNullptr (res);
            LOG ("Query execution is OK", DDEBUG);
            endTrx (emsg);
            return true;
        }

        throw schExcp (ERROR, "Query execution failed: " + emsg);

    } catch (exception &e) {
        if (res != nullptr)
            PQClearNullptr (res);

        emsg = e.what();
        throw;
    }
}
/*
 * execGet1DArray:
 *      This method execute the given SQL and returns the 1 dimensional string array.
 */
vector<string> *postgres::execGet1DArray (string query, string &emsg)
{
    PGresult *res = nullptr;

    try {
        beginTrx (emsg);
        LOG ("Executing Query: " + query, DDEBUG);
        res = PQexec (getPgConn(), query.c_str());

        if (!chkExecStatus (res, getPgConn(), emsg, true)) {
            throw schExcp (ERROR, emsg);

        } else if (PQntuples (res) == 0) {
            PQClearNullptr (res);
            endTrx (emsg);
            return nullptr;
        }

        vector<string> *result = new vector<string>;

        for (int f = 0; f < PQntuples (res); f++) {
            result->push_back (PQgetvalue (res, f, 0));
        }

        PQClearNullptr (res);
        endTrx (emsg);
        return result;

    } catch (exception &e) {
        emsg = PQerrorMessage (getPgConn());

        if (res != nullptr)
            PQClearNullptr (res);

        endTrx (emsg);
        throw;
    }
}
/*
 * escapeLiteral:
 *      This method do escape the literals in the given string.
 */
string postgres::escapeLiteral (string input)
{
    char *result = PQescapeLiteral (getPgConn(), input.c_str(), input.length());

    if (result == NULL)
        throw schExcp (ERROR, "Unable to perform escape literals on given input..");

    string out = string (result);
    // Clearing resources
    //
    PQfreemem (result);
    return out;
}
/*
 * execGetCSV:
 *      This method will execute the given SQL and return the CSV result as an output
 */
string postgres::execGetCSV (string query, string &err, pid_t &pid, bool isStoreColNames)
{
    PGresult *res = nullptr;

    try {
        beginTrx (err);
        LOG ("Executing Query: " + query, DDEBUG);
        pid = getDbPid();
        res = PQexec (getPgConn(), query.c_str());
        string result = "";

        if (PQresultStatus (res) != PGRES_TUPLES_OK && PQresultStatus (res) != PGRES_COMMAND_OK) {
            err = PQerrorMessage (getPgConn());
            PQClearNullptr (res);
            res = nullptr;
            throw schExcp (ERROR, "Unable to execute the query. Error: " + err);

        } else if (PQntuples (res) == 0) {
            LOG ("Query execution is OK", DDEBUG);
            PQClearNullptr (res);
            res = nullptr;
            return string();
        }

        // If store result column names flag is enabled,
        // then prepend the column names to the result.
        //
        if (isStoreColNames) {

            for (int t = 0; t < PQnfields (res); t++) {
                result += string (PQfname (res, t)) + ",";
            }

            rtrim (result, ',');
            // Adding an explicit new line, after the field name
            //
            result += "\n";
        }

        string tuples = "";

        for (int t = 0; t < PQntuples (res); t++) {
            tuples = "";

            for (int f = 0; f < PQnfields (res); f++)
                tuples += string (PQgetvalue (res, t, f)) + ",";

            rtrim (tuples, ',');
            result += tuples + "\n";
        }

        PQClearNullptr (res);
        endTrx (err);
        LOG ("Query Execution is OK", DDEBUG);
        return result;

    } catch (exception &e) {
        if (res != nullptr)
            PQClearNullptr (res);

        endTrx (err);
        throw;
    }
}
/*
 * execGet2DArray:
 *      This method executes the given SQL and returns the 2 dimensional string array.
 */
vector<vector<string>> postgres::execGet2DArray (string query, std::unordered_map<string, size_t> &fields, string &emsg)
{
    PGresult *res = nullptr;

    try {
        beginTrx (emsg);
        LOG ("Executing query: " + query, DDEBUG);
        res = PQexec (getPgConn(), query.c_str());

        if (!chkExecStatus (res, getPgConn(), emsg, true))
            throw schExcp (ERROR, "Query execution Failed: " + string (PQerrorMessage (getPgConn())));

        vector<vector<string>> result;

        for (int t = 0; t < PQntuples (res); t++) {
            vector<string> row;

            for (int f = 0; f < PQnfields (res); f++) {
                row.push_back (PQgetvalue (res, t, f));

                // Push all field names to a map
                //
                if (t == 0)
                    fields[string (PQfname (res, f))] = f;
            }

            result.push_back (row);
        }

        PQClearNullptr (res);
        endTrx (emsg);
        LOG ("Query execution is OK", DDEBUG);
        return result;

    } catch (exception &e) {
        emsg = e.what();

        if (res != nullptr)
            PQClearNullptr (res);

        endTrx (emsg);
        throw;
    }
}

void postgres::setPgConn (PGconn *con)
{
    pgDbConn = con;
}
/*
 * closeConnection:
 *      This method close the database connection.
 */
void postgres::closeConnection()
{
    if (getPgConn() != nullptr)
        PQfinish (getPgConn());

    setPgConn (nullptr);
    LOG ("Closed db connection.", DDEBUG);
}
/*
 * execStmt:
 *      This method executes the given SQL statement.
 */
void postgres::execStmt (string stmt, string &emsg)
{
    PGresult *res = nullptr;

    try {
        beginTrx (emsg);
        LOG ("Executing statement : " + stmt, DDEBUG);
        res = PQexec (getPgConn(), stmt.c_str());

        if (!chkExecStatus (res, getPgConn(), emsg, false))
            throw schExcp (ERROR, string (PQerrorMessage (getPgConn())));

        PQClearNullptr (res);
        endTrx (emsg);
        LOG ("Statement execution is OK", DDEBUG);

    } catch (exception &e) {
        emsg = e.what();

        if (res != nullptr)
            PQClearNullptr (res);

        endTrx (emsg);
        throw;
    }
}
/*
 * getDbPid:
 *      This method returns the database job pid.
 */
pid_t postgres::getDbPid()
{
    try {
        string emsg;
        pid_t pid = std::stoi (execGetScalar ("SELECT pg_backend_pid()", emsg));
        return pid;

    } catch (exception &e) {
        throw;
    }
}

/*
 * ~postgres:
 *      Default destructor of postgres object.
 */
postgres::~postgres()
{
    if (getPgConn() != nullptr) {
        closeConnection();
        PQfinish (getPgConn());
        LOG ("DB connection closed", DDEBUG);
    }
}
