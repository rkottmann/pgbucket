/*
 * pgBucket.cpp
 *
 *      This is the main resource file.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/pgBucket.cpp
 */
#include "../include/jobs.h"
#include "../include/jobQueue.h"
#include "../include/jobsRunner.h"
#include "../include/jobConfig.h"
#include "../include/bucketDaemon.h"

#include <unistd.h>
#include <getopt.h>
#include <cstdlib>
#include <memory>

// Scheduler's resource variable declarations.
//
postgres *centralConn = nullptr;
connPooler *dbConPool = nullptr;
hashTable *jHash = nullptr;
hashTable *eHash = nullptr;
schDbOps *schOps = nullptr;
configParams *settings = nullptr;

inline void Cleanup()
{
    if (centralConn != nullptr) delete centralConn;

    if (jHash != nullptr) delete jHash;

    if (eHash != nullptr) delete eHash;

    if (schOps != nullptr) delete schOps;

    if (settings != nullptr) delete settings;
}

// Globals
//
bool isDaemonMode = false;
string pidDir;
extern logger logWriter;
bool isDebug;
atomic<int> dispatchLimit;
atomic<useconds_t> dispatchDelay;

int main (int argc, char *argv[])
{
    // If enough arguments are not provided,
    // then exit from here.
    //
    if (argc < 2) {
        print_usage();
        exit (0);
    }

    // Utility variables
    //
    int nextOpt;
    bool isInsert = false, isDelete = false, isUpdate = false, isStatus = false, isDaemon = false, isNow = false;
    bool isStopDaemon = false, isInit = false, isDrop = false, isRefresh = false, isReload = false, isForceStop = false, isRunId = false, isExtended = false;
    runmode mode = BACKGROUND;
    string configFile;
    string  hostAddr, dbName, userName, dbPort, userPass, sockDir, jobId = "", nJobs = "10", runId = "";
    // Default number of columns to print in extended mode
    //
    uint extendedCols = 2;
    string nowAction;
    char status;
    const char *shrtOpts = "vhID:uiEOof:j:S:xQ:Rs:L:n:e:";
    const struct option longOpts[] = {
        {"help",             0,	NULL,	'h'},
        {"insert",           0,	NULL,	'i'},
        {"delete",           0,	NULL,	'x'},
        {"update",           0,	NULL,	'u'},
        {"init",             0,	NULL,	'I'},
        {"drop",             0,	NULL,	'O'},
        {"reload",           0,	NULL,	'o'},
        {"enabled",          0,	NULL,	'E'},
        {"configfile",       1,	NULL,	'f'},
        {"jobid",            1,	NULL,	'j'},
        {"status",           1,	NULL,	'S'},
        {"quitdaemon",       1,	NULL,	'Q'},
        {"startdaemon",      1,	NULL,	'D'},
        {"limit",            1, NULL,	'L'},
        {"version",          0,	NULL,	'v'},
        {"refresh",          0,	NULL,	'R'},
        {"serialid",         1,	NULL,	's'},
        {"now",              1,	NULL,	'n'},
        {"extended",         1, NULL,   'e'},
        {NULL,               0,	NULL,	0}
    };
    nextOpt = getopt_long (argc, argv, shrtOpts, longOpts, NULL);

    do {
        switch (nextOpt) {
            case 'h':
                print_usage();
                exit (0);
                break;

            case 'v':
                print_version();
                exit (0);
                break;

            case 'f':
                configFile = optarg;
                break;

            case 'j':
                jobId = optarg;
                break;

            case 'S':
                isStatus = true;
                status = *optarg;
                break;

            case 'n':
                isNow = true;
                nowAction = optarg;
                break;

            case 'i':
                isInsert = true;
                break;

            case 'x':
                isDelete = true;
                break;

            case 'u':
                isUpdate = true;
                break;

            case 'e':
                isExtended = true;
                int ncols;

                if (string2Int (string (optarg), ncols, 1))
                    extendedCols = (uint) ncols;
                else
                    exit (1);

                break;

            case 'D':
                isDaemon = true;

                if (optarg[0] == 'F')
                    mode = FOREGROUND;
                else if (optarg[0] == 'B')
                    mode = BACKGROUND;
                else
                    mode = INVALID_RUNMODE;

                break;

            case 'Q':
                isStopDaemon = true;

                if (optarg[0] == 'f')
                    isForceStop = true;
                else if (optarg[0] == 'n')
                    isForceStop = false;

                break;

            case 'L':
                nJobs = optarg;
                break;

            case 'I':
                isInit = true;
                break;

            case 'O':
                isDrop = true;
                break;

            case 'o':
                isReload = true;
                break;

            case 'R':
                isRefresh = true;
                break;

            case 's':
                isRunId = true;
                runId = optarg;
                break;

            default :
                printMsg ("Invalid option - " + std::to_string (nextOpt));
                printMsg ("Try --help to get valid options");
                exit (1);
        }
    } while ((nextOpt = getopt_long (argc, argv, shrtOpts, longOpts, NULL)) != -1);

    if (configFile.empty()) {
        if (!checkEnv ("PGBUCKET_CONFIG_FILE\0", configFile))
            exit (1);
    }

    // Parse the configuration file, and then initiate the configuration parameters.
    //
    settings = new configParams (configFile.c_str());

    // Try to parse and validate the configuration settings
    //
    if (! (settings->parseConfig() && settings->validateConfig())) {
        printMsg ("Configuration parsing is failed");
        Cleanup();
        exit (1);
    }

    // Enable the debug, as per the settings.
    //
    if (*settings->getConfigParam ("debug") == "on")
        isDebug = true;

    // Assign the settings to the local string variables
    //
    pidDir = *settings->getConfigParam ("pid_dir");
    sockDir = *settings->getConfigParam ("sock_dir");

    if (isStopDaemon) {
        int pid = isPgBucketRunning();

        try {
            if (pid == 0) {
                printMsg ("Unable to process " + rtrim (pidDir, '/') + "/pgBucket.pid file");
                throw schExcp (ERROR, "Failed to stop pgBucket daemon.");

            } else {
                if (isForceStop)
                    killPid (pid, SIGKILL);
                else
                    killPid (pid, SIGTERM);

                // Wait for 10ms, and check if the daemon still persists
                //
                usleep (10000);

                if (kill (pid, 0) == 0)
                    printMsg ("Seems, daemon is taking time to stop.");
                else {
                    printMsg ("pgBucket daemon is stopped successfully...");
                    unlink ((pidDir + "/pgBucket.pid").c_str());
                }
            }

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isReload) {
        int pid = isPgBucketRunning();

        try {
            if (pid == 0)
                throw schExcp (ERROR, "Unable to process " + rtrim (pidDir, '/') + "/pgBucket.pid file");
            else {
                printMsg ("Initiating sighup signal...");
                kill (pid, SIGHUP);
            }

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    // If daemon is running in foreground, then log messages into stdout
    //
    if (mode == FOREGROUND)
        logWriter.setStdout();

    else {
        auto logfile = settings->getConfigParam ("log_location");

        if (logfile != nullptr)
            logWriter.setLogFile (logfile);
    }

    if (isRefresh) {
        int pid = isPgBucketRunning();

        try {
            if (pid) {
                if (killPid (pid, SIGUSR2) == 0)
                    printMsg ("Initiated instance refresh..");

                Cleanup();
                exit (0);
            }

            throw schExcp (ERROR, "Unable to process " + rtrim (pidDir, '/') + "/pgBucket.pid file");

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    try {
        centralConn = new postgres (
            *settings->getConfigParam ("pgbucket_username"),
            *settings->getConfigParam ("pgbucket_password"),
            *settings->getConfigParam ("pgbucket_host_addr"),
            *settings->getConfigParam ("pgbucket_dbname"),
            POSTGRESQL,
            stoi (*settings->getConfigParam ("pgbucket_port")));
        jHash = new jobsHashTable (JOB);
        eHash = new evntsHashTable (EVENT);
        dbConPool = new connPooler (centralConn, stoi (*settings->getConfigParam ("pgbucket_dbpool_connections")));
        schOps = new schDbOps (dbConPool, jHash, eHash);

    } catch (exception &e) {
        e.what();
        Cleanup();
        exit (1);
    }

    if (isInit) {
        try {
            schOps->initCatalog();
            printMsg ("pgBucket catalog is created");
            Cleanup();
            exit (0);

        } catch (exception &e) {
            printMsg ("pgBucket catalog creation failed");
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isDrop) {
        string choice;
        printMsg ("CAUTION: Do you want to drop job catalog and collected job's information.");
        printMsg ("         It will delete job configuration details too.");
        printMsg ("         Press (Y) if you want to continue");
        std::cin >> choice;

        if (choice == "Y") {
            try {
                schOps->dropCatalog();
                printMsg ("pgBucket catalog is dropped");
                Cleanup();
                exit (0);

            } catch (exception &e) {
                printMsg ("pgBucket catalog is failed to drop");
                e.what();
                Cleanup();
                exit (1);
            }

        } else {
            printMsg ("Invalid choice..");
            Cleanup();
            exit (1);
        }
    }

    if (isInsert) {

        std::unique_ptr<jobConfig> jConf (new jobConfig (configFile.c_str(), schOps, settings));

        try {
            int totalJobs;
            jConf->parseConfig();
            jConf->pushJobs2Db (totalJobs);
            int pid = isPgBucketRunning();

            // If the number of rows got inserted/updated are >=1 then only raise the SIGUSR1 signal,
            // which will take care of refreshing the job/event hash.
            if (pid && totalJobs >= 1) {
                killPid (pid, SIGUSR1);
            }

            Cleanup();
            exit (0);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isUpdate) {
        std::unique_ptr<jobConfig> jConf (new jobConfig (configFile.c_str(), schOps, settings, isUpdate));
        int totalJobs;

        try {
            jConf->parseConfig (jobId);
            jConf->pushJobs2Db (totalJobs);
            int pid = isPgBucketRunning();

            if (pid && totalJobs >= 1) {
                killPid (pid, SIGUSR1);
            }

            Cleanup();
            exit (0);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isDelete) {
        try {
            if (jobId.empty())
                throw schExcp (ERROR, "Jobid is missing to delete");

            schOps->delJob (jobId);
            printMsg ("Job id ->" + jobId + " is deleted.");
            int pid = isPgBucketRunning();

            if (pid)
                killPid (pid, SIGUSR1);

            Cleanup();
            exit (0);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isStatus) {
        try {
            schOps->printStatJobs (status, nJobs, jobId, isExtended, extendedCols);
            Cleanup();
            exit (0);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isRunId) {
        try {
            schOps->printRunIdStats (runId, isExtended, extendedCols);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    if (isDaemon) {
        int fd = open ((pidDir + "/pgBucket.pid").c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

        try {
            if (fd == -1)
                throw schExcp (ERROR, "Unable to open the pid file." + string (strerror (errno)));

            // Trying to get pid file lock status
            //
            struct flock lckFile;
            lckFile.l_type = F_WRLCK;
            lckFile.l_whence = SEEK_SET;
            lckFile.l_start = 0;
            lckFile.l_len = 0;
            int lckStatus = getLckFileStatus (fd, lckFile);

            if (lckStatus == -1)
                throw schExcp (ERROR, "Unable to lock the pid file." + string (strerror (errno)));
            else if (lckStatus != 1)
                throw schExcp (ERROR, "pgBucket daemon is already running. Pid: " + std::to_string (lckStatus));

            isDaemonMode = true;

            if (mode == INVALID_RUNMODE)
                printMsg ("Invalid daemon run mode.");
            else {
                printMsg ("Starting pgBucket daemon...", true);
                bucketDaemon *daemon = new bucketDaemon (schOps, jHash, eHash, centralConn, settings, fd);
                daemon->runDaemon (mode);
            }

        } catch (exception &e) {
            e.what();
            Cleanup();
            close (fd);
            exit (1);
        }
    }

    if (isNow) {
        bool isJobIdMissing = false;

        if (nowAction == "") {
            printMsg ("Instant action should not be empty..");
            exit (1);
        }

        if (jobId == "") {
            isJobIdMissing = true;
            jobId = "NONE";
        }

        //Now, initiate client communication to daemon,
        //and print the responses.
        //
        try {
            socketClient sockClient (sockDir);
            sockClient.initClientSock();
            // Prepare a message to send to server
            //
            string msg = jobId;

            if (nowAction == "RN")
                msg += ":RUN_NORMAL";
            else if (nowAction == "RF")
                msg += ":RUN_FORCE";
            else if (nowAction == "E")
                msg += ":ENABLE";
            else if (nowAction == "D")
                msg += ":DISABLE";
            else if (nowAction == "SN")
                msg += ":STOP_NORMAL";
            else if (nowAction == "SF")
                msg += ":STOP_FORCE";
            else if (nowAction == "PQ" && !isExtended)
                msg += ":PRINT_JOB_QUEUE";
            else if (nowAction == "PQ" && isExtended)
                msg += ":EPRINT_JOB_QUEUE:" + std::to_string (extendedCols);
            else if (nowAction == "PCP" && !isExtended)
                msg += ":PRINT_CON_POOL";
            else if (nowAction == "PCP" && isExtended)
                msg += ":EPRINT_CON_POOL:" + std::to_string (extendedCols);
            else if (nowAction == "PH" && !isExtended)
                msg += ":PRINT_JOB_HASH";
            else if (nowAction == "PH" && isExtended)
                msg += ":EPRINT_JOB_HASH:" + std::to_string (extendedCols);
            else if (nowAction == "PEH" && !isExtended)
                msg += ":PRINT_EVENT_HASH";
            else if (nowAction == "PEH" && isExtended)
                msg += ":EPRINT_EVENT_HASH:" + std::to_string (extendedCols);
            else if (nowAction == "SKN")
                msg += ":SKIP_NEXT_RUN";
            else {
                printMsg ("Invalid action " + nowAction);
                Cleanup();
                exit (1);
            }

            //To print the whole job hash and job queue we do not need jobid.
            //Hence, validating other cases where jobid is required.
            //
            if (isJobIdMissing && (nowAction == "SKN" || nowAction == "SF" || nowAction == "SN" || nowAction == "RF"
                                   || nowAction == "RN" || nowAction == "E" || nowAction == "D")) {
                printMsg ("Jobid should not be empty.");
                Cleanup();
                exit (1);
            }

            sockClient.sendMsgToSocket (msg);
            string response = sockClient.recvMsgFromSocket();

            do {
                // Process and print messages, until socket receives "Bye" message
                //
                if (processSockResponse (response))
                    break;

                response = sockClient.recvMsgFromSocket();
            } while (1);

        } catch (exception &e) {
            e.what();
            Cleanup();
            exit (1);
        }
    }

    Cleanup();
    return 0;
}
