#include <array>
/*
 * bucketSocket.cpp
 *
 *      This source file implements the socket communication between daemon and cli options.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/bucketSocket.cpp
 */
#include "../include/bucketSocket.h"

/*
* socketServer:
*       Constructor of socketServer object
*/
socketServer::socketServer (string sDir)
{
    sockFile = sDir + ".pgBucket";
    sFd = cFd = 0;
    sockLen = sizeof sockAddr;
}

/*
* initServerSock:
*       This method initialize the server socket
*/
void socketServer::initServerSock()
{
    memset (&usaddr, 0, sizeof (usaddr));
#ifdef __APPLE__

    if ((sFd = socket (PF_LOCAL, SOCK_STREAM, 0)) == -1)
        throw schExcp (ERROR, "Socket initialization failed. Error: " + string (strerror (errno)));

    usaddr.sun_family = PF_LOCAL;
#else

    if ((sFd = socket (AF_UNIX, SOCK_STREAM, 0)) == -1)
        throw schExcp (ERROR, "Socket initialization failed. Error: " + string (strerror (errno)));

    usaddr.sun_family = AF_UNIX;
#endif
    strcpy (usaddr.sun_path, sockFile.c_str());
    usaddr.sun_path[sockFile.length()] = '\0';
    unlink (sockFile.c_str());

    if (::bind (sFd, (struct sockaddr *) &usaddr, sizeof (usaddr)) == -1)
        throw schExcp (ERROR, "Socket bind is failed. Error: " + string (strerror (errno)));
}

/*
* sendMessage:
*       This method send the given message to socket as server.
*/
void socketServer::sendMessage (string &msg)
{
    //Append a new line to given input, since we are delimiting the socket response with '\n'
    //
    send (cFd, (msg + "\n").c_str(), msg.length() + 1, 0);
}

/*
* listenAndProcessMsg:
*       This method listen to the socket and do process the messages, what it receives.
*/
void socketServer::listenAndProcessMsg (bucketDaemon *daemon)
{
    if (!disableSignals()) {
        LOG ("Unable to disable the maintenance signals to the pgBucket's socket listen method.", ERROR);
        return;
    }

    if (listen (sFd, MAX_LISTENS) == -1) {
        LOG ("Unable to listen on the daemon socket. Error: " + string (strerror (errno)), ERROR);
        return;
    }

    while (1) {
        sockLen = sizeof (sockAddr);

        if ((cFd = accept (sFd, (sockaddr *)&ClientDetails, &sockLen)) < 0)
            throw schExcp (ERROR, "Unable to accept a connection to socket. Error: " + string (strerror (errno)));
        else {
            char recvBuf[100] = {};
            size_t n;

            while ((n = read (cFd, recvBuf, (size_t) (sizeof (recvBuf) - 1))) > 0) {
                // Setting proper null terminator at the end of the received string
                //
                recvBuf[n] = '\0';
                string recMsg = string (recvBuf);
                string sendMsg = "";
                LOG ("Got " + recMsg + " from the local socket.", DDEBUG);
                daemonActions daemonAction;
                size_t jId;

                try {
                    if (recMsg.substr (0, recMsg.find_first_of (':')) != "NONE")
                        jId = stoi (recMsg.substr (0, recMsg.find_first_of (':')));
                    else
                        jId = 0;

                } catch (exception &e) {
                    sendMsg = string (e.what());
                }

                if (sendMsg != "") {
                    LOG ("Found an error, during the process of parsing the received message.", ERROR);
                    sendMessage (sendMsg);
                    break;
                }

                string action = recMsg.substr (recMsg.find_first_of (':') + 1);
                int extendedCols = 2;

                if (action == "RUN_NORMAL")
                    daemonAction = RUN_NORMAL;
                else if (action == "RUN_FORCE")
                    daemonAction = RUN_FORCE;
                else if (action == "STOP_NORMAL")
                    daemonAction = STOP_NORMAL;
                else if (action == "STOP_FORCE")
                    daemonAction = STOP_FORCE;
                else if (action == "SKIP_NEXT_RUN")
                    daemonAction = SKIP_NEXT_RUN;
                else if (action == "PRINT_JOB_HASH")
                    daemonAction = PRINT_JOB_HASH;
                else if (action == "EPRINT_JOB_HASH")
                    daemonAction = EPRINT_JOB_HASH;
                else if (action == "PRINT_EVENT_HASH")
                    daemonAction = PRINT_EVENT_HASH;
                else if (action.find ("EPRINT_EVENT_HASH") != std::string::npos) {
                    daemonAction = EPRINT_EVENT_HASH;
                    string2Int (action.substr (action.find_first_of (':') + 1), extendedCols);

                } else if (action == "PRINT_JOB_QUEUE")
                    daemonAction = PRINT_JOB_QUEUE;
                else if (action.find ("EPRINT_JOB_QUEUE") != std::string::npos) {
                    daemonAction = EPRINT_JOB_QUEUE;
                    string2Int (action.substr (action.find_first_of (':') + 1), extendedCols);

                } else if (action == "PRINT_CON_POOL")
                    daemonAction = PRINT_CON_POOL;
                else if (action.find ("EPRINT_CON_POOL") != std::string::npos) {
                    daemonAction = EPRINT_CON_POOL;
                    string2Int (action.substr (action.find_first_of (':') + 1), extendedCols);

                } else if (action == "ENABLE")
                    daemonAction = ENABLE;
                else if (action == "DISABLE")
                    daemonAction = DISABLE;
                else
                    daemonAction = INVALID_ACTION;

                if (daemonAction == INVALID_ACTION)
                    sendMsg += "Invalid job daemon action found";

                if (sendMsg != "")
                    sendMessage (sendMsg);
                else {
                    LOG ("Got the daemon action " + action + " for the Job id ->" + std::to_string (jId), NOTICE);
                    daemon->doActions (daemonAction, jId, extendedCols);
                    sendMsg = "Bye";
                    // Sending Bye message to client, to close the connection
                    //
                    sendMessage (sendMsg);
                }
            }
        }

        close (cFd);
    }
}

/*
* socketClient:
*       Constructor of socketClient object.
*/
socketClient::socketClient (string sockDir)
{
    strcpy (localSock.sun_path, (sockDir + ".pgBucket").c_str());
    localSock.sun_path[ (sockDir + ".pgBucket").length()] = '\0';
    cFd = 0;
}
/*
* initClientSock:
*       This method initializes the client socket.
*/
void socketClient::initClientSock()
{
#ifdef __APPLE__

    if ((cFd = socket (PF_LOCAL, SOCK_STREAM, 0)) == -1) {
        throw schExcp (ERROR, "Unable to initiate local socket communication. Error: " + string (strerror (errno)));
    }

    localSock.sun_family = PF_LOCAL;
#else

    if ((cFd = socket (AF_UNIX, SOCK_STREAM, 0)) == -1) {
        throw schExcp (ERROR, "Unable to initiate local socket communication. Error: " + string (strerror (errno)));
    }

    localSock.sun_family = AF_UNIX;
#endif
    LOG ("Trying to connect local socket...", DDEBUG);

    if (connect (cFd, (struct sockaddr *)&localSock, sizeof (localSock)) == -1) {
        throw schExcp (ERROR, "Unable to connect the local socket. " + string (strerror (errno)));
    }

    LOG ("Connected to local socket..", DDEBUG);
}

/*
* sendMsgToSocket:
*       This method send messate to socket as client
*/
void socketClient::sendMsgToSocket (string msg)
{
    if (send (cFd, msg.c_str(), msg.length(), 0) == -1) {
        throw schExcp (ERROR, "Unable to send the given message( " + msg + " to local socket. Error: " + string (strerror (errno)));
    }
}
/*
* recvMsgFromSocket:
*       This method recieve message from socket as client.
*/
string socketClient::recvMsgFromSocket()
{
    ssize_t recvBytesLen;
    char recvMsg[MAX_RECV_BYTES] = {};
    string msg = "";

    if ((recvBytesLen = recv (cFd, recvMsg, (size_t) MAX_RECV_BYTES, 0)) > 0) {
        recvMsg[recvBytesLen] = '\0';
        msg = string (recvMsg);
        return msg;

    } else if (recvBytesLen == -1)
        throw schExcp (ERROR, "Unable to receive the message from the server. Error: " + string (strerror (errno)));
    else
        throw schExcp (ERROR, "Server closed the socket.");
}

/*
* ~socketClient:
*       Destructor of socketClient object.
*/
socketClient::~socketClient()
{
    close (cFd);
}
