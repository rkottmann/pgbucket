/*
 * logger.cpp
 *
 *      This source file has the implementation of logger methods.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/logger.cpp
 */
#include "../include/logger.h"
#include "../include/jobConfig.h"

logger logWriter;

extern string parse_log_levels (logLevels loglevel);
extern void printMsg (string msg, bool);
extern bool isDaemonMode;
extern bool isDebug;

logger::logger()
{
    out = nullptr;
}

/*
 * setLogFile:
 *      This method initiate the logger file.
 */
void logger::setLogFile (const string *logfile)
{
    logf.open (logfile->c_str(), std::ios::out | std::ios::app);

    if (logf.fail()) {
        printMsg ("Unable to locate the given log file " + *logfile, false);
        exit (1);
    }

    out = &logf;
}
/*
 * setStdout:
 * 		This method re-directs the output stream to stdout.
 */
void logger::setStdout()
{
    out = &std::cout;
}
/*
 * writeLogContent:
 *      This method write the messages into log file.
 */
bool logger::writeLogContent (const stringstream &msg, const logLevels logLevel = DETAIL, const char *srcfile, int linenum)
{
    // If the daemon is not running in debug mode,
    // then skip all the DEBUG messages to write into log file
    //
    if (!isDebug) {
        if (logLevel == DDEBUG)
            return true;
    }

    // If the pgBucket is used for the utility operations like "print job status", "start/stop daemon", e.t.c
    // in those cases we don't need to print the messages in LOG file.
    //
    if (!isDaemonMode) {
        printMsg (msg.str(), false);
        return true;
    }

    // If the output stream is not set, then return from here.
    //
    if (out == nullptr) {
        return true;

    } else {
        LckGaurd lock (lockStream);
        time_t t = time (0);
        struct tm *time = localtime (&t);
        *out
                << time->tm_year + 1900 << "/"
                << time->tm_mon + 1 << "/"
                << time->tm_mday
                << " "
                << time->tm_hour << ":"
                << time->tm_min << ":"
                << time->tm_sec
                << " "
                << time->tm_zone
                << " "
                << std::left << std::setw (8) << std::setfill (' ') << parse_log_levels (logLevel)
                << msg.str();

        if (srcfile != nullptr)
            *out << " (" << srcfile << ":" << linenum << ")";

        *out << std::endl;
        out->flush();
        return true;
    }
}
/*
 * ~logger:
 *      Default destructor of the logger object.
 */
logger::~logger()
{
    LckGaurd lock (lockStream);
}
