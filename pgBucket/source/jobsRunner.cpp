/*
 * jobsRunner.cpp
 *
 *      This source file has the implementation of the job dispatcher.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/jobsRunner.cpp
 */
#include "../include/jobsRunner.h"
extern atomic<long> dispatchDelay;
extern atomic<int> dispatchLimit;
extern atomic<bool> isSigterm;

/*
 * jobsRunner:
 * 		Default constructor to jobsRunner
 */
jobsRunner::jobsRunner()
{
    jobIds = nullptr;
    jobHash = nullptr;
    eventHash = nullptr;
    jobQ = nullptr;
    dbConPool = nullptr;
}
/*
 * jobsRunner:
 *      Parameterized constructor to jobsRunner object
 */
jobsRunner::jobsRunner (vector<jobIdPosPair> *jIds, hashTable *jobs, hashTable *events, jobsQueue *jQ, connPooler *cPool)
{
    jobIds = jIds;
    jobHash = jobs;
    eventHash = events;
    jobQ = jQ;
    dbConPool = cPool;
}
/*
 * dispatch:
 *      This method dispatch all the collected list of jobs
 */
bool jobsRunner::dispatch()
{
    jobIdPosPair jId;
    uint i = 0;

    for (i = 0; i < jobIds->size() && !isSigterm; i++) {
        try {
            jId = jobIds->at (i);

            // Yes, we are done getting all the jobIds from the vector
            //
            if (jId.jobId == 0)
                break;

        } catch (const std::out_of_range &e) {
            LOG ("Dispatcher is looking for more job ids, which are > MAX_THREAD setting", ERROR);
            LOG (e.what(), ERROR);
            continue;
        }

        LOG ("Dispatching Job id -> " + std::to_string (jId.jobId), DDEBUG);
        thread jobWorker (runIt(), jId, jobHash, eventHash, jobQ, dbConPool);
        jobWorker.detach();

        // If the number of dispatch count is greater than dispatch_cnt, then wait of dispatch_delay milliseconds
        //
        if ((i + 1) % dispatchLimit == 0) {
            LOG ("Dispatched " + std::to_string (dispatchLimit) + " jobs. Hence, sleeping for " + std::to_string (dispatchDelay) + " ms", NOTICE);
            usleep ((useconds_t) (1000 * dispatchDelay));
        }
    }

    if (isSigterm)
        LOG ("Received halt signal, hence not dispatching the pending " + std::to_string (jobIds->size() - i) + " jobs.", NOTICE);

    return true;
}

void parseCmdTags(string& cmd, jobs *parentJob, jobs *job, hashTable *eHash, hashTable *jHash) {
    // Look for a specific job's property, and then replace it with the corresponding values
    //
    smatch match;
    string propVal, jobIdStr, jobProp;
    jobs *aJob = nullptr;
    int jobPos = -1;
        
    while (1) {
        regex_search (cmd, match, regex_str);
        
        if (match.empty())
            break;
        
        // Check whether the job property belong to it's parent job or to any specific job
        //
        if (!match[1].str().empty()) {
            jobIdStr = match[1].str();
            jobProp = match[2].str();
            
            // Trim @ symbol from the jobid string
            //
            rtrim (jobIdStr, '@');
            
            // Check whether it's a event or normal job
            //
            int jid;
            string2Int (jobIdStr, jid);
            
            jobPos = eHash->getJobIdPos ((size_t) jid);
            
            if (jobPos == -1) {
                jobPos = jHash->getJobIdPos ((size_t) jid);
                
                if (jobPos != -1)
                    aJob = jHash->getJob ((size_t) jobPos);
                
            } else
                aJob = eHash->getJob ((size_t) jobPos);
            
        } else {
            aJob = parentJob;
            jobProp = match[2].str();
        }
        
        if (aJob == nullptr)
            throw schExcp (ERROR, "Unable to find the Job id -> " + jobIdStr + " while processing the Job id ->" + std::to_string (job->getJobID()));
        
        // Get the job's property values
        //
        if (jobProp == "__pname__")
            propVal = aJob->getJobName();
        else if (jobProp == "__pjid__")
            propVal = std::to_string (aJob->getJobID());
        else if (jobProp == "__ppid__")
            propVal = std::to_string (aJob->getJobPid());
        else if (jobProp == "__perror__")
            propVal = aJob->getJobError();
        else if (jobProp == "__presult__")
            propVal = aJob->getJobResult();
        else if (jobProp == "__pruncnt__")
            propVal = std::to_string (aJob->getJobRunCounter());
        else if (jobProp == "__pissuccess__")
            propVal = aJob->getJobSuccess() ? "true" : "false";
        else if (jobProp == "__pschstatus__")
            propVal = aJob->getSchStatusStr();
        else
            throw schExcp (ERROR, "Invalid job property " + jobProp + " found while processing the Job id ->" + std::to_string (job->getJobID()));
        
        // Trim if we have any new lines at the end of error or result values
        //
        trim (propVal, '\n');
        
        // Let us update the job cmd with the proper value
        //
        
        cmd.replace (match.position(), match.length(), propVal);
    }
}

void runJob (jobs *parentJob, jobs *job, hashTable *eHash, hashTable *jHash, connPooler *dbConPool, string &execPath)
{
    string result, errmsg, runSeqNo;
    pid_t pid;
    bool isSuccess = false;
    string cmd = job->getJobCmd();
    time_t stime, etime;

    if (job == nullptr)
        throw schExcp (ERROR, "Unable to process the event job, which is empty");

    // Before running an event job, we need to make sure that the job is enabled.
    // If by chance it is disabled/deleted, then return from here.
    //
    if (!job->getJobStatus() || job->isJobDeleted()) {
        LOG ("Unable to run the event Job id->" + std::to_string (job->getJobID()) + " which is disabled.", WARNING);
        return;
    }

    try {
        job->lockJob();
        string cmd = job->getJobCmd();

        // Parse the event job command arguments.
        //
        if (job->getIsParseCmdParams()) {
            parseCmdTags(cmd, parentJob, job, eHash, jHash);
        }

        job->initJobSettings();
        runSeqNo = dbConPool->execGetScalar ("INSERT INTO _schedule_pgbucket_.jobstatus(jobid, jobname, jobcmd, status) VALUES(" + std::to_string (job->getJobID()) + ", '" + job->getJobName() + "', " + dbConPool->escapeLiteral (cmd) + ", 'R') RETURNING sno", errmsg);

        if (runSeqNo.empty())
            throw schExcp (ERROR, "Unable to record the event Job id ->" + std::to_string (job->getJobID()) + " running status in database.");

        LOG ("Event Job id ->" + std::to_string (job->getJobID()) + " is processing...", NOTICE);
        job->setJobEndTime (0);
        job->setJobSchStatus (RUNNING);
        execPath += std::to_string (job->getJobID()) + "->";
        stime = getMyClockEpoch();

        string oldCmd = job->getJobCmd();

        if (job->getIsParseCmdParams() && cmd != oldCmd) {
            // Update the job command with new updated command
            //
            job->setJobCmd (cmd);
        }

        // Run the job here
        //
        try {

            isSuccess = job->runJob (result, errmsg, pid);

            if (!job->getJobFailIfOutput().empty() && result == job->getJobFailIfOutput()) {
                // Job result match with `JOBFAILIFRESULT` property
                //
                isSuccess = false;
                errmsg += "Job result is match with it's JOBFAILIFRESULT property";
            }

        } catch (exception &e) {
            errmsg = e.what();
        }

        etime = getMyClockEpoch();

        // Reset the job command with it's previous
        //
        if (job->getIsParseCmdParams() && cmd != oldCmd)
            job->setJobCmd (oldCmd);

        cmd.clear();
        oldCmd.clear();
        if (!isSuccess) {
            LOG ("Unable to process Event Job id ->" + std::to_string (job->getJobID()) + ", and error is : " + errmsg, NOTICE);
            job->failJobSettings();
            job->incJobFailCounter();

            // If the job fail count reaches to DISABLEIFFAILCNT, then disable the job
            //
            if (job->getDisableFailCnt() > 0 && job->getJobFailCnt() >= job->getDisableFailCnt()) {
                LOG ("Auto disabling the Job id->" + std::to_string (job->getJobID()) + " as it reaches it's disable failure count", NOTICE);
                job->setJobStatus (false);
            }


        } else {
            LOG ("Event Job id ->" + std::to_string (job->getJobID()) + " is completed with PID(" + std::to_string (pid) + ") with duration " + std::to_string (etime - stime) + " seconds.", NOTICE);
            job->passJobSettings();
        }

        job->setJobResult (result);
        job->setJobError (errmsg);
        job->unlockJob();

        // Store the event job results into DB
        //
        dbConPool->execDMLstmt ("UPDATE _schedule_pgbucket_.jobstatus SET jobendtime = now(), result = " + dbConPool->escapeLiteral (result) + ", error = " +
                                dbConPool->escapeLiteral (errmsg) + ", status = '" + (isSuccess ? "S" : "E") + "', jobpid = " + std::to_string (pid) + " WHERE sno = " + runSeqNo, errmsg);

        // Based on the previous job execution status, let us run the other event jobs
        //
        if (isSuccess) {
            size_t *eJobIds = job->getJobPassEvtIds();

            if (eJobIds != nullptr)
                for (uint i = 0; eJobIds[i]; i++) {
                    jobs *eJob = eHash->getJob (eHash->getJobIdPos (eJobIds[i]));
                    job->setJobSchStatus (RUNNING_EVENTJOB);
                    runJob (job, eJob, eHash, jHash, dbConPool, execPath);

                    if (job->getJobSchStatus() != KILLED)
                        job->setJobSchStatus (COMPLETED);
                }

        } else {
            size_t *eJobIds = job->getJobFailEvtIds();

            if (eJobIds != nullptr)
                for (uint i = 0; eJobIds[i]; i++) {
                    jobs *eJob = eHash->getJob (eHash->getJobIdPos (eJobIds[i]));
                    job->setJobSchStatus (RUNNING_EVENTJOB);
                    runJob (job, eJob, eHash, jHash, dbConPool, execPath);

                    if (job->getJobSchStatus() != KILLED)
                        job->setJobSchStatus (COMPLETED);
                }
        }

    } catch (exception &e) {
        job->unlockJob();
        job->failJobSettings();
        throw;
    }
}

/*
 * operator():
 *      This is a functor which actually calls the job's run method
 */
void runIt::operator() (jobIdPosPair jId, hashTable *jHTable, hashTable *eHTable, jobsQueue *jobQ, connPooler *dbConPool, runMode mode)
{
    string result, errmsg, runSeqNo, execPath;
    long nextRun = -1;

    // Blocking maintenance signals for this thread.
    //
    if (!disableSignals()) {
        LOG ("Unable to disable the maintenance signals for this Job id ->" + std::to_string (jId.jobId), ERROR);
        return;
    }

    if (jHTable == nullptr) {
        LOG ("Job hash is empty to get and process the Job id ->" + std::to_string (jId.jobId), ERROR);
        return;

    } else if (jHTable->getHashCounter() == 0) {
        LOG ("Job hash has 0 entries to get and process the Job id ->" + std::to_string (jId.jobId), ERROR);
        return;
    }

    jobs *job = jHTable->getJob (jId.hashPos);

    if (job == nullptr) {
        LOG ("Job is empty to process the Job id ->" + std::to_string (jId.jobId), ERROR);
        return;
    }

    if (mode != EXPLICIT_FORCE && (job->isJobDeleted() || !job->getJobStatus())) {
        job->lockJob();
        LOG ("Job id ->" + std::to_string (job->getJobID()) + " is deleted/disabled..", DDEBUG);

        // Check whether jobQ is empty or not. If not, then only remove the item from the queue.
        // A set of jobs may fail to delete from the jobQ, during the daemon SIGTERM handling.
        //
        if (jobQ->getFront() != nullptr)
            jobQ->delJobFrmQueue (jId.jobId);

        job->unlockJob();
        return;
    }

    if (mode != EXPLICIT_FORCE && job->getJobSkipNextRun()) {
        job->lockJob();
        LOG ("Job id ->" + std::to_string (job->getJobID()) + " has enabled skip next run flag. Hence, skipping it to run..", NOTICE);
        job->setJobSkipNextRun (false);
        job->setJobSchStatus (SKIPPED);
        //As we are skipping the job's next run, we should calculate it's next run and
        //update the jobQ with it's next run
        //
        jobQ->delJobFrmQueue (jId.jobId);

        if (!jHTable->getIsReSyncInProgress()) {
            time_t currEpoch = getMyClockEpoch();
            nextRun = jobQ->setJobNextRun (jHTable->getJob (jId.hashPos), currEpoch);

            if (nextRun >= 0 ) {
                jobQ->insJob2Queue (jId, currEpoch, nextRun);
            }
        }

        job->unlockJob();
        return;
    }

    try {
        time_t currEpoch;
        job->lockJob();
        job->initJobSettings();

        // Before dispatching job, let us remove the job id from the queue,
        // and re-calculate the job next run. Once the job execution is completed, then
        // push it back to JobQ
        //
        if (mode == INTERNAL) {
            // Don't disturb the jobQ with external job run invokes
            //
            jobQ->delJobFrmQueue (job->getJobID());
        }

        string cmd = job->getJobCmd();
        runSeqNo = dbConPool->execGetScalar ("INSERT INTO _schedule_pgbucket_.jobstatus(jobid, jobname, jobcmd, status) VALUES(" + std::to_string (job->getJobID()) + ", '" + job->getJobName() + "', " + dbConPool->escapeLiteral (cmd) + ", 'R') RETURNING sno", errmsg);

        if (runSeqNo.empty())
            throw schExcp (ERROR, "Unable to record the Job id ->" + std::to_string (job->getJobID()) + " running status in database.");

        LOG ("Job id ->" + std::to_string (job->getJobID()) + " is processing...", NOTICE);
        job->setJobEndTime (0);

        // Do not update the job's next run when we run any job externally..
        //
        if (mode == INTERNAL) {
            job->setJobNextRun (0);
        }

        job->setJobSchStatus (RUNNING);
        time_t startTime, endTime;
        bool isRunSuccess = false;
        pid_t pid;
        startTime = getMyClockEpoch();

        if (! (job->getJobType() == OSLEVEL || job->getJobType() == DBLEVEL))
            throw schExcp (ERROR, "Invalid job type found for the Job id ->" + std::to_string (job->getJobID()) + " during it's dispatch");

        try {
            isRunSuccess = job->runJob (result, errmsg, pid);

            if (!job->getJobFailIfOutput().empty() && result == job->getJobFailIfOutput()) {
                isRunSuccess = false;
                errmsg += "Job result is match with it's JOBFAILIFRESULT property";
            }

        } catch (exception &e) {
            e.what();
            isRunSuccess = false;
        }

        endTime = getMyClockEpoch();

        if (isRunSuccess)
            job->passJobSettings();
        else {
            job->failJobSettings();
            job->incJobFailCounter();

            // If the job fail count reaches to DISABLEIFFAILCNT, then disable the job
            //
            if (job->getDisableFailCnt() > 0 && job->getJobFailCnt() >= job->getDisableFailCnt()) {
                LOG ("Auto disabling the Job id->" + std::to_string (job->getJobID()) + " as it reaches it's disable failure count", NOTICE);
                job->setJobStatus (false);
            }
        }

        // Check whether job got kill request explicitly
        //
        if (job->getJobSchStatus() != KILLED)
            job->setJobSchStatus (RUNNING_EVENTJOB);

        job->setJobResult (result);
        job->setJobError (errmsg);
        execPath += "START->" + std::to_string (job->getJobID()) + "->";

        try {
            if (isRunSuccess && job->getJobPassEvtIds() != nullptr && eHTable != nullptr) {
                auto eJobId = job->getJobPassEvtIds();
                jobs *eJob = eHTable->getJob (eHTable->getJobIdPos (eJobId[0]));
                runJob (job, eJob, eHTable, jHTable, dbConPool, execPath);

            } else if (!isRunSuccess && job->getJobFailEvtIds() != nullptr && eHTable != nullptr) {
                auto eJobId = job->getJobFailEvtIds();
                jobs *eJob = eHTable->getJob (eHTable->getJobIdPos (eJobId[0]));
                runJob (job, eJob, eHTable, jHTable, dbConPool, execPath);
            }

        } catch (exception &e) {
            e.what();
        }

        execPath += "END";
        job->setExecPath (execPath);

        if (!isRunSuccess)
            LOG ("Unable to process Job id ->" + std::to_string (jId.jobId) + ". Execution path is: " + execPath, NOTICE);
        else
            LOG ("Job id ->" + std::to_string (jId.jobId) + " is completed with PID(" + std::to_string (pid) + ") with duration " + std::to_string (endTime - startTime) + " seconds. Execution path is: " + execPath, NOTICE);

        if (job->getJobSchStatus() != KILLED)
            job->setJobSchStatus (COMPLETED);


        // This is a case, where a user run a job explicitly by using -nR -j <jobId> command
        // In that case, don't re-calculate it's next run and don't push this job into jobQ.
        //
        if (mode != INTERNAL) {
            LOG ("Job id ->" + std::to_string (job->getJobID()) + " is invoked explicitly, hence not scheduling it again.", NOTICE);
        }


        // If instance refresh starts do not calculate the jobs nextRun.
        // Since, refresh process gives a brand new jobs hash and jobQ instances.
        //
        else if (!jHTable->getIsReSyncInProgress()) {

            if (job->getJobStatus()) {
                // Calculating job's next run again as per new clock epoch
                //
                currEpoch = getMyClockEpoch();
                nextRun = jobQ->setJobNextRun (jHTable->getJob (jId.hashPos), currEpoch);

                if (nextRun >= 0 ) {
                    jobQ->insJob2Queue (jId, currEpoch, nextRun);
                    job->setJobSchStatus (SCHEDULED);
                }

            } else
                LOG ("Unable to push Job id->" + std::to_string (job->getJobID()) + " into queue as it is in disable status.", WARNING);

        } else
            LOG ("Daemon is either shutting down or doing a refresh. Hence skipping the job's next run calculation", DDEBUG);

        //Escaping string quotes, backslashes, ...
        //
        dbConPool->execDMLstmt ("UPDATE _schedule_pgbucket_.jobstatus SET jobendtime = now(), result = " + dbConPool->escapeLiteral (result) + ", error = " +
                                dbConPool->escapeLiteral (errmsg) + ", status = '" + (isRunSuccess ? "S" : "E") + "', jobpid = " + std::to_string (pid) +
                                ", jobnextrun = " + (nextRun < 0 ? "NULL" : "NOW() + '" + std::to_string (nextRun) + " seconds'::interval") + ",execpath =" + dbConPool->escapeLiteral (execPath) +
                                " WHERE sno = " + runSeqNo, errmsg);
        //Unlocking job
        //
        job->unlockJob();

    } catch (exception &e) {
        job->unlockJob();
        job->failJobSettings();
        e.what();
    }
}
