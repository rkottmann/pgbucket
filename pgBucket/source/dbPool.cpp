/*
 * dbPool.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: dinesh
 */
#include "../include/dbPool.h"

connPooler::connPooler()
{
    conPool = nullptr;
    dbPoolCount = 0;
}

connPooler::connPooler (postgres *con, int cnt)
{
    dbPoolCount = cnt;

    if (dbPoolCount > 0) {
        conPool = new vector<postgres *>;

    } else {
        conPool = nullptr;
        return;
    }

    // Acquire a pool lock, and then populate the connections into the vector.
    //
    LckGaurd lock (poolLock);

    for (int i = 0; i < dbPoolCount; i++) {
        LOG ("Pushing connection #" + std::to_string (i) + " into pool.", DDEBUG);
        postgres *newCon = new postgres (con->getDbUserName(), con->getDbMd5Password(), con->getDbHostIp(), con->getDbName(), POSTGRESQL, con->getDbPort());

        if (newCon->getPgConn() == nullptr) {
            delete newCon;
            throw schExcp (ERROR, "Unable to push a connection into pool.");
        }

        conPool->push_back (newCon);
    }
}

postgres *connPooler::getConnFromPool()
{
    postgres *con = nullptr;

    while (1) {
        poolLock.lock();

        if (!conPool->empty()) {
            con = conPool->back();
            conPool->pop_back();
            break;

        } else
            LOG ("Connection pool is empty at this moment, and will return a new connection when pool is filled.", DDEBUG);

        poolLock.unlock();
        usleep (100000); // Waiting 100ms before checking the pool status
    }

    poolLock.unlock();
    return con;
}

void connPooler::pushConnIntoPool (postgres *con)
{
    if (con == nullptr)
        LOG ("Unable to push nullptr into the connection pool", ERROR);
    else {
        poolLock.lock();
        conPool->push_back (con);
        poolLock.unlock();
    }
}

void connPooler::clearPool()
{
    LckGaurd lock (poolLock);

    for (int i = 0; i < dbPoolCount; i++) {
        LOG ("Closing the pool connection #" + std::to_string (i), DDEBUG);
        postgres *con = conPool->back();
        conPool->pop_back();
        con->closeConnection();
        delete con;
    }
}

int connPooler::getPoolCount() const
{
    return dbPoolCount;
}

int connPooler::getActiveCount()
{
    return (int) (dbPoolCount - conPool->size());
}

int connPooler::getAvailableCount()
{
    return (int) conPool->size();
}

string connPooler::printConnPoolState (bool isExtended, uint numcols)
{
    vector<string> fields;
    vector<vector<string>> tuples;
    vector<string> row;
    fields.push_back ("Used count");
    fields.push_back ("Available count");
    row.push_back (std::to_string (getActiveCount()));
    row.push_back (std::to_string (getAvailableCount()));
    tuples.push_back (row);
    return printTable ("Connection Pool State", tuples, fields, isExtended, numcols);
}


vector<string> *connPooler::execGet1DArray (string qry, string &emsg)
{
    postgres *con = getConnFromPool();

    try {
        vector<string> *result = con->execGet1DArray (qry, emsg);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}


vector<vector<string>> connPooler::execGet2DArray (string qry, std::unordered_map<string, size_t> &fields, string &emsg)
{
    postgres *con = getConnFromPool();

    try {
        vector<vector<string>> result = con->execGet2DArray (qry, fields, emsg);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}

string connPooler::execGetScalar (string qry, string &emsg)
{
    postgres *con = getConnFromPool();

    try {
        string result = con->execGetScalar (qry, emsg);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}

void connPooler::execStmt (string qry, string &emsg)
{
    postgres *con = getConnFromPool();

    try {
        con->execStmt (qry, emsg);
        pushConnIntoPool (con);

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}

string connPooler::execGetCSV (string qry, string &err, pid_t &pid)
{
    postgres *con = getConnFromPool();

    try {
        string result = con->execGetCSV (qry, err, pid);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}

string connPooler::escapeLiteral (string input)
{
    postgres *con = getConnFromPool();

    try {
        string result = con->escapeLiteral (input);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}

bool connPooler::execDMLstmt (string qry, string &err)
{
    postgres *con = getConnFromPool();

    try {
        bool result = con->execDMLstmt (qry, err);
        pushConnIntoPool (con);
        return result;

    } catch (exception &e) {
        pushConnIntoPool (con);
        throw;
    }
}


connPooler::~connPooler()
{
    // Clear the pool connections
    //
    if (conPool != nullptr) {
        clearPool();
        delete conPool;
        conPool = nullptr;
    }
}



