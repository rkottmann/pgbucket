/*
 * pgBucket logger declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/logger.h
 */
#ifndef INCLUDE_LOGGER_H_
#define INCLUDE_LOGGER_H_
#include <iostream>
#include <iomanip>
#include "alias.h"

typedef enum logLevels { DDEBUG, DETAIL, HINT, WARNING, ERROR, NOTICE } logLevels;

class logger
{

    private:
        ofstream logf;
        mutex lockStream;
        ostream *out;

    public:
        logger();
        void setLogFile (const string *logfile);
        void setStdout();
        bool writeLogContent (const stringstream &logMessage, const logLevels logLevel, const char *srcfile = nullptr, int linenum = -1);
        ~logger();
};

extern logger logWriter;

#endif
