/*
 * pgBucket daemon class declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/bucketDaemon.h
 */

#ifndef INCLUDE_BUCKETDAEMON_H
#define INCLUDE_BUCKETDAEMON_H

#include "jobQueue.h"
#include "jobsRunner.h"
#include "schDbOps.h"
#include "bucketSocket.h"
#include "jobConfig.h"
#include "dbPool.h"

#include <sys/stat.h>
#include <sys/file.h>
#include <sys/ioctl.h>
class socketServer;
//TODO:
// Implement a sub action method, which avoid the duplicate of pritnting table responses.
//
typedef enum {ENABLE, DISABLE, RUN_NORMAL, RUN_FORCE, STOP_NORMAL, STOP_FORCE, SKIP_NEXT_RUN, PRINT_JOB_HASH, PRINT_EVENT_HASH, PRINT_JOB_QUEUE, PRINT_CON_POOL, EPRINT_JOB_HASH, EPRINT_EVENT_HASH, EPRINT_JOB_QUEUE, EPRINT_CON_POOL, INVALID_ACTION} daemonActions;

class bucketDaemon
{
    private:
        string sockDir;
        static long secSpend;
        static schDbOps *schOps;
        static hashTable *jHash;
        static hashTable *eHash;
        static vector<postgres *> centralConnPool;
        static jobsRunner *jRunner;
        static jobsQueue *jobQ;
        static connPooler *dbConPool;
        static socketServer *sockServer;
        static string pidFileLoc;
        static mutex resourceLocker;
        static pthread_t maintThreadId;
        static int pidFd;
    public:
        bucketDaemon (schDbOps *, hashTable *, hashTable *, postgres *, configParams *, int pidFd);
        static void handleIntrKill (int sig);
        static void doUpdateHash (int sig);
        static void reFresh (int sig);
        static void reloadSettings (int sig);
        static void updateBuckets();
        static void pushBucketPointers (jobsQueue **, jobsQueue **, jobSchBuckets frmSchBucket, jobSchBuckets toSchBucket);
        static void doActions (daemonActions action, size_t jobId, int extendedCols = 2);
        void daemonIt();
        void runDaemon (runmode mode);
        static void start();
        void daemonInitStuff();
        ~bucketDaemon();
};

#endif //INCLUDE_BUCKETDAEMON_H
