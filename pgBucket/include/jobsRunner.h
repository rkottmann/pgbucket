/*
 * pgBucket job dispatcher declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/jobsRunner.h
 */

#ifndef INCLUDE_JOBSRUNNER_H_
#define INCLUDE_JOBSRUNNER_H_

#include "jobQueue.h"
#include "dbpref.h"
#include "dbPool.h"

static const char* regex_pattern = "([1-9]+@)?(__pname__|__pjid__|__ppid__|__perror__|__presult__|__pruncnt__|__pissuccess__|__pschstatus__)";
static regex regex_str(regex_pattern);

typedef enum runMode { INTERNAL, EXPLICIT_NORMAL, EXPLICIT_FORCE } runMode;
void runJob (jobs *job, hashTable *eHash, hashTable *jHash, connPooler *dbConPool, string &execPath);
void parseCmdTags (string& cmd, jobs *parentJob, jobs *job, hashTable *eHash, hashTable *jHash);

class jobsRunner
{

    private:
        vector<jobIdPosPair> *jobIds;
        hashTable *jobHash;
        hashTable *eventHash;
        jobsQueue	*jobQ;
        connPooler *dbConPool;

    public:
        jobsRunner();
        jobsRunner (vector<jobIdPosPair> *, hashTable *, hashTable *, jobsQueue *, connPooler *dbConPool);
        bool dispatch();
};


// Functor to runJobs through threads
//
class runIt
{

    public:
        void operator() (jobIdPosPair jId, hashTable *jHTable, hashTable *eHTable, jobsQueue *jobQ, connPooler *dbConPool, runMode mode = INTERNAL);
};

#endif /* INCLUDE_JOBSRUNNER_H_ */
