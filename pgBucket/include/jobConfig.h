﻿/*
 * pgBucket configuration object declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/jobConfig.h
 */

#ifndef INCLUDE_JOBCONFIG_H_
#define INCLUDE_JOBCONFIG_H_


#include "schDbOps.h"
#include <iostream>
#include <strings.h>
#include <map>
#include <unordered_set>

const string jobFileArgs[] = {
    "JOBID",
    "JOBNAME",
    "ENABLE",
    "JOBRUNFREQ",
    "JOBCLASS",
    "JOBTYPE",
    "CMD",
    "DBCONN",
    "JOBPASSEVNTS",
    "JOBFAILEVNTS",
    "DISABLEIFFAILCNT",
    "JOBFAILIFRESULT",
    "PARSECMDPARAMS",
    "RECDBRESCOLNAMES"
};

const string config_params[][5] = {

    // SETTING                      SUPPORTED VALUES         DEFAULT VALUE           DATA TYPE          RELOAD
    {"sock_dir",                    "",                      "/tmp/",                "string",          "n"},
    {"pid_dir",                     "",                      "/tmp/",                "string",          "n"},
    {"pgbucket_host_addr",          "",                      "127.0.0.1",            "string",          "n"},
    {"pgbucket_dbname",             "",                      "postgres",             "string",          "n"},
    {"pgbucket_username",           "",                      "postgres",             "string",          "n"},
    {"pgbucket_port",               "",                      "5432",                 "int",             "n"},
    {"pgbucket_password",           "",                      "postgres",             "string",          "n"},
    {"pgbucket_dbpool_connections", "",                      "1",                    "int",             "n"},
    {"log_location",                "",                      "/tmp/",                "string",          "n"},
    {"child_process_mode",          "fork_exec,posix_spawn", "fork_exec",            "string",          "y"},
    {"debug",                       "on,off",                "off",                  "string",          "y"},
    {"dispatch_limit",              "",                      "10",                   "int",             "y"},
    {"dispatch_delay",              "",                      "10",                   "int",             "y"},
    {"",                            "",                      "",                     "",                ""}
};
typedef enum config_cat {CONFIG, JOBS, INVALID_CAT} config_cat;
typedef enum config_checks {VALID, INVALID_KEY, INVALID_VALUE, INVALID_SETTING} config_checks;

class keyValue
{
    private:
        string key, value;
    public:
        string getKey();
        string getValue();
        void convertStr2KV (string line);
};

class configParams
{
    private:
        std::map<string, string> confParams;
        ifstream conFileStream;
        string conFile;
        std::streamoff jobSectionLinePos;
        mutex lockConfig;
    public:
        configParams (const char *file);
        void setConfigParam (string param, string setting, bool overWrite);
        const string *getConfigParam (const string &param);
        void deleteAllConfigParam();
        void getDefaultParam (string param);
        bool parseConfig (bool isReload = false);
        bool reloadSettings();
        bool validateConfig();
        void openConfStream();
        void closeConfStream();
        config_checks isValidConfParam (const char *param, const char *val, bool &isReloadable);
        config_cat getConfType (string confType);
        std::streamoff getJobSectionLinePos();
        ~configParams();
};

class jobConfig
{
    private:
        ifstream		conFile;
        const char	*fileLoc;
        int				totalJobs;
        std::map<string, string> jobs;
        schDbOps		*schOps;
        configParams	*confParams;
        bool			isUpdate;	// Perform Update of a job, with the config entries
    public:
        jobConfig (const char *file, schDbOps *schOps,  configParams *confParams, bool isUpdate = false);
        bool parseConfig (string jobid = "");
        bool pushJobs2Db (int &toaljobs);
        bool isValidJobArg (const char *arg);
        void prepareJobFreqEle (string val, string eletype, int from, int to, size_t lineNum);
};

#endif
